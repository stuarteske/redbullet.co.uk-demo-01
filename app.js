//<debug>
Ext.Loader.setPath({
    'Ext': 'sdk/src'
});
//</debug>

Ext.application({
    name: 'rbdemo',
	
    requires: [
        'Ext.MessageBox'
    ],
	
	models: [
		'ActivityTimelineEntity',
		'ClientEntity',
		'EventDate',
		'Settings',
		'StatisticAppointmentEntity',
		'AchievementBookingsThisWeek'
	],
	
	stores: [
		'ActivityTimelineData',
		'ClientData',
		'EventSummaryData',
		'ClientActivityTimelineData',
		'Settings',
		'StatisticAppointmentData',
		'AchievementBookingsThisWeekData'
	],
	
	controllers: [
		'Events',
		'Clients',
		'Activity',
		'Settings'
	],
	
    views: [
    	'Main',
    	'SettingsYourProfile',
    	'SettingsAddService',
    	'SettingsSocialToolsFacebookPanel',
    	'SettingsSocialToolsTwitterPanel',
    	'SettingsViewService',
    	'SettingsViewServiceSprayTan',
    	'ActivityStatisticsViewPanel',
    	'ActivityAchievementViewPanel',
    	'ActivityAchievementFacebookViewPanel',
    	'ActivityAchievementTwitterViewPanel'
    ],

    icon: {
        57: 'resources/icons/Icon.png',
        72: 'resources/icons/Icon~ipad.png',
        114: 'resources/icons/Icon@2x.png',
        144: 'resources/icons/Icon~ipad@2x.png'
    },
    
    phoneStartupScreen: 'resources/loading/Homescreen.jpg',
    tabletStartupScreen: 'resources/loading/Homescreen~ipad.jpg',
	
    launch: function() {
    	// Make the activity Filter Global
    	var activityFilter = Ext.create('Ext.Panel', {
			id: 'idActivityFilterPanel',
			width    : window.innerWidth / 1.2,
			height   : window.innerHeight / 1.2,
			scrollalbe:true,
			modal: true,
			centered : true,
			hideOnMaskTap: true,
			layout:'card',
			items: [
				{ 	
					xtype: 'formpanel',
					items: [
						{
							xtype: 'fieldset',
							title: 'Select Filter',
							//instructions: 'Close by touching the select button or by touching outside of this panel.',
							items: [
								{
									xtype: 'radiofield',
									name : 'filter',
									value: '1',
									label: 'All',
									checked: true,
									labelWidth:'70%',
								},
								{
									xtype: 'radiofield',
									name : 'filter',
									value: '2',
									label: 'Statistics',
									labelWidth:'70%',
								},
								{
									xtype: 'radiofield',
									name : 'filter',
									value: '3',
									label: 'Calendar Alerts',
									labelWidth:'70%',
								},
								{
									xtype: 'radiofield',
									name : 'filter',
									value: '4',
									label: 'General Alerts',
									labelWidth:'70%',
								},
								{
									xtype: 'radiofield',
									name : 'filter',
									value: '5',
									label: 'Achievements',
									labelWidth:'70%',
									listeners: {
									   change: {
										   fn: function() {
											   console.log( 'Changed' );
										   }
									   }
									}
								}
							]
						},
						{
							xtype: 'fieldset',	
							items: [
								{
									xtype:'button',
									text: 'Select',
									ui: 'confirm',
									scope: this,
									handler: function() {
										//console.log('select');
										activityFilter.hide();
									}
								}
							]
						},
					]
				},
			]
		});
    	Ext.Viewport.add(activityFilter.hide());
    	
    	// Make the activity Filter Global
    	var eventStatus = Ext.create('Ext.form.Panel', {
			id: 'idEventsStatusPanel',
			itemId: 'itemIdEventsStatusFormPanel',
			width    : window.innerWidth / 1.2,
			height   : window.innerHeight / 1.2,
			scrollalbe:true,
			modal: true,
			centered : true,
			hideOnMaskTap: true,
			layout:'card',
			items: [
				{ 	
					xtype: 'formpanel',
					items: [
						{
							xtype: 'fieldset',
							title: 'Select Filter',
							//instructions: 'Close by touching the select button or by touching outside of this panel.',
							items: [
								{
									xtype: 'radiofield',
									name : 'filter',
									value: '1',
									label: 'Unconfirmed',
									checked: true,
									labelWidth:'70%',
								},
								{
									xtype: 'radiofield',
									name : 'filter',
									value: '2',
									label: 'Confirmed',
									labelWidth:'70%',
								},
								{
									xtype: 'radiofield',
									name : 'filter',
									value: '3',
									label: 'Canceled',
									labelWidth:'70%',
								},
								{
									xtype: 'radiofield',
									name : 'filter',
									value: '4',
									label: 'In Progress',
									labelWidth:'70%',
								},
								{
									xtype: 'radiofield',
									name : 'filter',
									value: '5',
									label: 'Complete',
									labelWidth:'70%',
								}
							]
						},		
						{
							xtype: 'fieldset',	
							items: [
								{
									xtype:'button',
									text: 'Apply',
									ui: 'confirm',
									scope: this,
									handler: function() {
										//console.log('select');
										eventStatus.hide();
									}
								}
							]
						},
					]
				},
			]
		});
    	Ext.Viewport.add(eventStatus.hide());
    	
    	// Event type selection panel
    	var eventType = Ext.create('Ext.Panel', {
			id: 'idEventTypePanel',
			width    : window.innerWidth / 1.2,
			height   : 145,
			scrollalbe:true,
			modal: true,
			centered : true,
			hideOnMaskTap: true,
			layout:'card',
			items: [
				{ 	
					xtype: 'formpanel',
					items: [
						{
							xtype: 'fieldset',	
							items: [
								{
									xtype:'button',
									itemId: 'appointmentBtn',
									text: 'Create Appointment',
									ui: 'normal',
									scope: this,
									handler: function() {
										//console.log('select');
										eventType.hide();
									}
								}
							]
						},
						{
							xtype: 'fieldset',	
							items: [
								{
									xtype:'button',
									itemId: 'blockoutBtn',
									text: 'Block Out Availability ',
									ui: 'normal',
									scope: this,
									handler: function() {
										//console.log('select');
										eventType.hide();
									}
								}
							]
						},
					]
				},
			]
		});
    	Ext.Viewport.add(eventType.hide());
    	
    	rbdemo.globals = {
		   key: activityFilter,
		   key2: '1', // All
		   key3: eventStatus,
		   key4: '1', // Unconfirmed
		   key5: eventType,
		   key6: '1'
		};
    
        // Destroy the #appLoadingIndicator element
        Ext.fly('appLoadingIndicator').destroy();

        // Initialize the main view
        Ext.Viewport.add(Ext.create('rbdemo.view.Main'));
    },

    onUpdated: function() {
        Ext.Msg.confirm(
            "Application Update",
            "This application has just successfully been updated to the latest version. Reload now?",
            function() {
                window.location.reload();
            }
        );
    },
});