Ext.define('rbdemo.controller.Clients', {
    extend: 'Ext.app.Controller',
    
    requires: [
    	'rbdemo.view.ClientsMainNavigationView',
    	'rbdemo.view.ClientsCreateClientFormPanel',
    	'rbdemo.view.ClientsInitialPanel',
    	'rbdemo.view.ClientsViewClientPanel',
    	'rbdemo.view.ClientsEditClientFormPanel',
    	'rbdemo.view.EventsViewEventPanelCarrAldred'
    ],
    
    // Launch
    launch: function(app) {
        this.callParent(arguments);
        //console.log("Event Controller Launched");
    },
    
    // Init
	init: function () {
		this.callParent(arguments);
		//console.log("Controller Inited");
	},
    
    config: {
        refs: {
        	// Clients Navigation View
        	navigationView: 'xtypeClientsMainNavicationView',
        	navigationViewCreateClientButton: 'xtypeClientsMainNavicationView #idNavigationCreateClient',
        	navigationViewEditClientButton: 'xtypeClientsMainNavicationView #idNavigationEditClient',
        	navigationViewAddEventButton: 'xtypeEventsMainNavicationView #idNavigationAddEvent',
        	navigationViewEditEventButton: 'xtypeEventsMainNavicationView #idNavigationEditEvent',
        	// Clients Toolbar
        	clientSegmentcontrol: 'xtypeClientsViewClientPanel segmentedbutton',
        	// Create Client Form
        	createClientForm: 'xtypeClientsCreateClientFormPanel',
        	createClientFormBtn: 'xtypeClientsCreateClientFormPanel button[ui=confirm]',
        	getClientFormBtn: 'xtypeClientsCreateClientFormPanel button[ui=action]',
        	// Client Group List
        	clientGroupList: 'xtypeClientsGroupListingViewPanel list',
        	// Client List
        	clientList: 'xtypeClientsInitialPanel list',
        	// Client View
        	clientDetailsPanel: 'xtypeClientsViewClientPanel',
        	clientDetailsPanelSubNavigation: 'xtypeClientsViewClientPanel #idClientDetailsNavigationContainer',
        	clientDetailsActivityList: 'xtypeClientsViewClientPanel list',
        	// Client Edit Form
        	clientEditFormPanel: 'xtypeClientsEditClientFormPanel',
        	clientEditEventFormBtn: 'xtypeClientsEditClientFormPanel button[ui=confirm]',
        	clientDeleteEventFormBtn: 'xtypeClientsEditClientFormPanel button[ui=decline]',
        	// Note Create Form
        	notesPhotoPickerFormPanelBtn: 'xtypeNotesCreateNoteFormPanel #itemIdPhotoPickerBtn',
        	notesCreateFormPanelBtn: 'xtypeNotesCreateNoteFormPanel button[ui=confirm]',
        },
        control: {
        	// Navigation Bar
        	navigationView: {
        		activeitemchange: 'onNavigationViewActiveItemChange'
        	},
        	navigationViewEditClientButton: {
        		tap: 'onTapNavigationViewEditClientBtn'
        	},
        	getClientFormBtn: {
        		tap: 'onTapGetClientFormBtn'
        	},
        	createClientFormBtn: {
        		tap: 'onTapCreateClientFormBtn'
        	},
        	// Client Group List
        	clientGroupList: {
        		itemtap: 'onClientGroupListItemTap'
        	},
        	// Client List
        	clientList: {
        		itemtap: 'onClientListItemTap'
        	},
        	// Client View
        	clientDetailsActivityList: {
        		itemtap: 'onItemTapClientDetailsActivityList'
        	},
        	/*'xtypeClientsViewClientPanel': {
				tap: 'onPanelTap'
			},*/
        	// Client Edit Form
        	clientEditEventFormBtn: {
        		tap: 'onTapClientEditEventFormBtn'
        	},
        	clientDeleteEventFormBtn: {
        		tap: 'onTapClientDeleteEventFormBtn'
        	},
        	// Notes Create
        	notesPhotoPickerFormPanelBtn: {
        		tap: 'onTapNotesPhotoPickerBtn'
        	},
        	notesCreateFormPanelBtn: {
        		tap: 'onTapNotesCreateBtn'
        	},
        }
    },
    
    // Commands.
    onNavigationViewActiveItemChange: function( container, value, oldValue, eOpts) {
    	switch (value.xtype) {
    		case 'xtypeClientsCreateClientFormPanel':
    			this.getNavigationViewCreateClientButton().setHidden(true);
    			this.getNavigationViewEditClientButton().setHidden(true);
    			break;
    		case 'xtypeClientsViewClientPanel':
    			this.getNavigationViewCreateClientButton().setHidden(true);
    			this.getNavigationViewEditClientButton().setHidden(false);
    			break;
    		case 'xtypeEventsCreateEventFormPanel':
    			// Hide the foreign Event and Create Button
    			this.getNavigationViewAddEventButton().setHidden(true);
    			this.getNavigationViewEditEventButton().setHidden(true);
    			// Hide the native edit button
    			this.getNavigationViewEditClientButton().setHidden(true);
    			break;
    		case 'xtypeEventsViewEventPanel':
    			// Hide the foreign Event and Create Button
    			this.getNavigationViewAddEventButton().setHidden(true);
    			this.getNavigationViewEditEventButton().setHidden(true);
    			// Hide the native edit button
    			this.getNavigationViewEditClientButton().setHidden(true);
    			break;
    		case 'xtypeEventsViewEventPanelCarrAldred':
    			// Hide the foreign Event and Create Button
    			this.getNavigationViewAddEventButton().setHidden(true);
    			this.getNavigationViewEditEventButton().setHidden(true);
    			// Hide the native edit button
    			this.getNavigationViewEditClientButton().setHidden(true);
    			break;
    		case 'xtypeNotesCreateNoteFormPanel':
    			this.getNavigationViewCreateClientButton().setHidden(true);
    			this.getNavigationViewEditClientButton().setHidden(true);
    			break;
    		default:
    			this.getNavigationViewCreateClientButton().setHidden(false);
    			this.getNavigationViewEditClientButton().setHidden(true);
    			break;
    	}
    },
    onTapNavigationViewEditClientBtn: function(button, e, eOpts) {
    	this.getNavigationView().push({xtype:'xtypeClientsEditClientFormPanel'});
    },
    onTapGetClientFormBtn: function(button, e, eOpts) {
    	Ext.Msg.alert('Get Client Info', 'This would allow you to select a contact from the device\'s address book.', function(e) {});
	},
	onTapCreateClientFormBtn: function(button, e, eOpts) {
		var controller = this;
		
		Ext.Msg.confirm('Confirmation', 'Are you sure that you would like to create this client?', function(e) {
			if (e == 'yes') {
				// Confirmed
				controller.getNavigationView().pop();
			} else {
				// Denied
				//controller.testFunction();
			}
		});
	},
	// Client Group List
	onClientGroupListItemTap: function(dataview, index, target, record, e, options) {
		this.getNavigationView().push({xtype:'xtypeClientsInitialPanel'});
	},
	// Client List
	onClientListItemTap: function(dataview, index, target, record, e, options) {
		this.getNavigationView().push({xtype:'xtypeClientsViewClientPanel'});
	},
	// Client View Panel
	onItemTapClientDetailsActivityList: function(dataview, index, target, record, e, options) {
		switch (index) {
			case 0:
				this.getNavigationView().push({xtype:'xtypeEventsViewEventPanelCarrAldred'});
				break;
			case 1:
				this.getNavigationView().push({xtype:'xtypeEventsViewEventPanelCarrAldred'});
				break;
			case 2:
				this.getNavigationView().push({xtype:'xtypeEventsViewEventPanelCarrAldred'});
				break;
			case 3:
				this.getClientDetailsPanelSubNavigation().setActiveItem(2);
				this.getClientSegmentcontrol().setPressedButtons([2]);
				break;
			default:
				break;
		}
	},
	/*onPanelTap: function(button, e, eOpts) {
		//image = a.getTarget("#imageId");
		//if (image) {
			console.log(eOpts);
		//}
	},*/
	// Client Edit Form
	onTapClientEditEventFormBtn: function(button, e, eOpts) {
		var controller = this;
		
		Ext.Msg.confirm('Confirmation', 'Are you sure that you would like to save these changes?', function(e) {
			if (e == 'yes') {
				// Confirmed
				controller.getNavigationView().pop();
			} else {
				// Denied
				//controller.testFunction();
			}
		});
	},
	onTapClientDeleteEventFormBtn: function(button, e, eOpts) {
		var controller = this;
		
		Ext.Msg.confirm('Confirmation', 'Are you sure that you would like to delete this client?', function(e) {
			if (e == 'yes') {
				// Confirmed
				controller.getNavigationView().reset();
			} else {
				// Denied
				//controller.testFunction();
			}
		});
	},
	// Notes Create
    onTapNotesPhotoPickerBtn: function(button, e, eOpts) {
    	Ext.Msg.alert('Photo Picker', 'This will allow you to pick photo to attach to the note.', function(e) {});
    },
    onTapNotesCreateBtn: function(button, e, eOpts) {
    	var controller = this;
		
		Ext.Msg.confirm('Confirmation', 'Are you sure that you would like to create this client note?', function(e) {
			if (e == 'yes') {
				// Confirmed
				controller.getNavigationView().pop();
			} else {
				// Denied
				//controller.testFunction();
			}
		});
    },
    testFunction: function() {
    	console.log('Test Function');
    },
});