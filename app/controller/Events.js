Ext.define('rbdemo.controller.Events', {
    extend: 'Ext.app.Controller',
    
    requires: [
    	'rbdemo.view.EventsPerDayListPanel',
    	'rbdemo.view.EventsInitialPanel',
    	'rbdemo.view.EventsMainNavigationView',
    	'rbdemo.view.EventsCreateEventFormPanel',
    	'rbdemo.view.EventsViewEventPanel',
    	'rbdemo.view.EventsEditEventFormPanel',
    	'rbdemo.view.EventsCreateBlockoutFormPanel',
    	'rbdemo.view.EventsPerDayViewPanelBlockedout',
    	'rbdemo.view.EventsEditBlockoutFormPanel',
    	'rbdemo.view.EventsCompleteEventFormPanel',
    	'rbdemo.view.EventsInProgressEventFormPanel'
    ],
    
    // Launch
    launch: function(app) {
        this.callParent(arguments);
        //console.log("Event Controller Launched");
    },
    
    // Init
	init: function () {
		this.callParent(arguments);
		//console.log("Controller Inited");
	},
    
    config: {
        refs: {
        	// Navigation View
        	navigationView: 'xtypeEventsMainNavicationView',
        	navigationBar: '#idEventsNavigationBar',
        	navigationViewAddEventButton: 'xtypeEventsMainNavicationView #idNavigationAddEvent',
        	navigationViewEditEventButton: 'xtypeEventsMainNavicationView #idNavigationEditEvent',
        	// Event Type Panel
        	eventTypeAppointmentBtn: '#idEventTypePanel #appointmentBtn',
        	eventTypeBlockoutBtn: '#idEventTypePanel #blockoutBtn',
        	// Initial Event List
        	initialEventsList: 'xtypeEventsInitialPanel list',
        	// Events Create Form
        	addEventForm: 'xtypeEventsCreateEventFormPanel',
        	addEventFormBtn: 'xtypeEventsCreateEventFormPanel button[ui=confirm]',
        	// Events View Panel
        	eventsDayList: 'xtypeEventsDayListPanel list',
        	// Events Edit Form
        	editEventForm: 'xtypeEventsEditEventFormPanel',
        	editEventFormBtn: 'xtypeEventsEditEventFormPanel button[ui=confirm]',
        	deleteEventFormBtn: 'xtypeEventsEditEventFormPanel button[ui=decline]',
        	// Blocked Out List
        	eventsBlockoutList: 'xtypeEventsPerDayViewPanelBlockedout list',
        	// Create Block Out
        	createBlockoutFormBtn: 'xtypeEventsCreateBlockoutFormPanel button[ui=confirm]',
        	// Edit Block Out
        	updateBlockoutFormBtn: 'xtypeEventsEditBlockoutFormPanel button[ui=confirm]',
        	deleteBlockoutFormBtn: 'xtypeEventsEditBlockoutFormPanel button[ui=decline]',
        	// Event Status
        	eventStatusForm: '#itemIdEventsStatusFormPanel',
        	eventStatusConfirmFormBtn: '#idEventsStatusPanel button[ui=confirm]',
        	// Event Complete Form
        	eventCompleteFormBtn: 'xtypeEventsCompleteEventFormPanel button[ui=confirm]',
        	// Event In Progress Form
        	eventInProgressFormBtn: 'xtypeEventsInProgressEventFormPanel button[ui=confirm]',
        },
        control: {
        	// Navigation Bar
        	navigationView: {
        		activeitemchange: 'onNavigationViewActiveItemChange'
        	},
        	navigationViewAddEventButton: {
        		tap: 'onTapAddEventNavigationButton',
        	},
        	navigationViewEditEventButton: {
        		tap: 'onTapEditEventNavigationButton',
        	},
        	// Event Type Panel
        	eventTypeAppointmentBtn: {
        		tap:'onTapEventTypeAppointmentBtn',
        	},
        	eventTypeBlockoutBtn: {
        		tap:'onTapEventTypeBlockoutBtn',
        	},
        	// Add Event Form
        	addEventFormBtn: {
        		tap: 'onTapAddEventFormBtn',
        	},
        	// Edit Event Form
        	editEventFormBtn: {
        		tap: 'onTapEditEventFormBtn',
        	},
        	deleteEventFormBtn: {
        		tap: 'onTapDeleteEventFormBtn',
        	},
        	// Inital List
        	initialEventsList: {
        		itemtap: 'onInitialEventsListItemTap',
        	},
        	// Event by Day List
        	eventsDayList: {
        		itemtap: 'onEventsDayListItemTap',
        	},
        	// Blocked out List
        	eventsBlockoutList: {
        		itemtap: 'onItemTapEventsBlockoutList',
        	},
        	// Create Block Out
        	createBlockoutFormBtn: {
        		tap: 'onTapCreateBlockoutFormBtn',
        	},
        	// Edit Block Out 
        	updateBlockoutFormBtn: {
        		tap: 'onTapUpdateBlockoutFormBtn',
        	},
        	deleteBlockoutFormBtn: {
        		tap: 'onTapDeleteBlockoutFormBtn',
        	},
        	// Event Status
        	eventStatusConfirmFormBtn: {
        		tap: 'onTapEventStatusConfirmFormBtn',
        	},
        	// Event Complete Form
        	eventCompleteFormBtn: {
        		tap: 'onTapEventCompleteFormBtn',
        	},
        	// Event In Progress Form
        	eventInProgressFormBtn: {
        		tap: 'onTapEventInProgressFormBtn',
        	},
        }
    },
    
    // Commands.
    onNavigationViewActiveItemChange: function( container, value, oldValue, eOpts) {
    	switch (value.xtype) {
    		case 'xtypeEventsCreateEventFormPanel':
    			this.getNavigationViewAddEventButton().setHidden(true);
    			this.getNavigationViewEditEventButton().setHidden(true);
    			break;
    		case 'xtypeEventsViewEventPanel':
    			this.getNavigationViewAddEventButton().setHidden(true);
    			this.getNavigationViewEditEventButton().setHidden(false);
    			break;
    		case 'xtypeEventsEditEventFormPanel':
    			this.getNavigationViewAddEventButton().setHidden(true);
    			this.getNavigationViewEditEventButton().setHidden(true);
    			break;
    		case 'xtypeEventsPerDayViewPanelBlockedout':
    			this.getNavigationViewAddEventButton().setHidden(true);
    			this.getNavigationViewEditEventButton().setHidden(true);
    			break;
    		case 'xtypeEventsEditBlockoutFormPanel':
    			this.getNavigationViewAddEventButton().setHidden(true);
    			this.getNavigationViewEditEventButton().setHidden(true);
    			break;
    		case 'xtypeEventsInProgressEventFormPanel':
    			this.getNavigationViewAddEventButton().setHidden(true);
    			this.getNavigationViewEditEventButton().setHidden(true);
    			break;
    		case 'xtypeEventsCompleteEventFormPanel':
    			this.getNavigationViewAddEventButton().setHidden(true);
    			this.getNavigationViewEditEventButton().setHidden(true);
    			break;
    		default: 
    			this.getNavigationViewAddEventButton().setHidden(false);
    			this.getNavigationViewEditEventButton().setHidden(true);
    			break;
    	}
    },
    onTapAddEventNavigationButton: function() {
    	rbdemo.globals['key5'].show();
    },
    onTapEditEventNavigationButton: function() {
    	this.getNavigationView().push({xtype:'xtypeEventsEditEventFormPanel'});
    },
    // Event Type Panel
    onTapEventTypeAppointmentBtn: function(button, e, eOpts) {
    	this.getNavigationView().push({xtype:'xtypeEventsCreateEventFormPanel'});
    },
    onTapEventTypeBlockoutBtn: function(button, e, eOpts) {
    	this.getNavigationView().push({xtype:'xtypeEventsCreateBlockoutFormPanel'});
    },
    onTapAddEventFormBtn: function(button, e, eOpts) {
    	var controller = this;
    	var formValues = this.getAddEventForm().getValues()
    	//console.log(this.getAddEventForm().getValues());
    	
    	Ext.Msg.confirm('Confirmation', 'Are you sure that you would like to book Carr Aldred in for a Spray Tan?', function(e) {
			if (e == 'yes') {
				// Confirmed
				controller.getNavigationView().pop();
			} else {
				// Denied
				//controller.testFunction();
			}
		});
    },
    onTapEditEventFormBtn: function(button, e, eOpts) {
    	var controller = this;
    	
    	Ext.Msg.confirm('Confirmation', 'Are you sure that you would like to apply these changes?', function(e) {
			if (e == 'yes') {
				// Confirmed
				controller.getNavigationView().pop();
			} else {
				// Denied
				//controller.testFunction();
			}
		});
    },
    onTapDeleteEventFormBtn: function(button, e, eOpts) {
    	var controller = this;
    	
    	Ext.Msg.confirm('Confirmation', 'Are you sure that you would like to delete this event?', function(e) {
			if (e == 'yes') {
				// Confirmed
				controller.getNavigationView().pop();
			} else {
				// Denied
				//controller.testFunction();
			}
		});
    },
    onInitialEventsListItemTap: function(dataview, index, target, record, e, options) {
    	switch (index) {
    		case 14:
    			this.getNavigationView().push({xtype:'xtypeEventsPerDayViewPanelBlockedout'});
    			break;
    		default:	
    			this.getNavigationView().push({xtype:'xtypeEventsDayListPanel'});
    			break;
    	}
    },
    onEventsDayListItemTap: function(dataview, index, target, record, e, options) {
    	this.getNavigationView().push({xtype:'xtypeEventsViewEventPanel'});
    },
    // Blocked out List
    onItemTapEventsBlockoutList: function(dataview, index, target, record, e, options) {
    	this.getNavigationView().push({xtype:'xtypeEventsEditBlockoutFormPanel'});
    },
    // Create Block Out
    onTapCreateBlockoutFormBtn: function(button, e, eOpts) {
    	var controller = this;
    	
    	Ext.Msg.confirm('Confirmation', 'Are you sure that you would like to block out this period of availability?', function(e) {
			if (e == 'yes') {
				// Confirmed
				controller.getNavigationView().pop();
			} else {
				// Denied
				//controller.testFunction();
			}
		});
    },
    // Edit Block Out
    onTapUpdateBlockoutFormBtn: function(button, e, eOpts) {
    	var controller = this;
    	
    	Ext.Msg.confirm('Confirmation', 'Are you sure that you would like to edit this blocked out availability?', function(e) {
			if (e == 'yes') {
				// Confirmed
				controller.getNavigationView().pop();
			} else {
				// Denied
				//controller.testFunction();
			}
		});
    },
    onTapDeleteBlockoutFormBtn: function(button, e, eOpts) {
    	var controller = this;
    	
    	Ext.Msg.confirm('Confirmation', 'Are you sure that you would like to delete this blocked out availability?', function(e) {
			if (e == 'yes') {
				// Confirmed
				controller.getNavigationView().pop();
			} else {
				// Denied
				//controller.testFunction();
			}
		});
    },
    // Event Status
    onTapEventStatusConfirmFormBtn: function(button, e, eOpts) {
    	var controller = this;
    	var formValue = controller.getEventStatusForm().getValues().filter;
    	
    	switch(formValue) {
    		case '4':
    			//console.log('Changing Page');
    			this.getNavigationView().push({xtype:'xtypeEventsInProgressEventFormPanel'});
    			break;
    		case '5':
    			this.getNavigationView().push({xtype:'xtypeEventsCompleteEventFormPanel'});
    			break;
    		default:
    			break;
    	}
    	
    	//console.log(formValues.filter);
    },
    // Event Complete Form
	onTapEventCompleteFormBtn: function(button, e, eOpts) {
		this.getNavigationView().pop();
	},
	// Event In Progress Form
	onTapEventInProgressFormBtn: function(button, e, eOpts) {
		this.getNavigationView().pop();
	},
    testFunction: function() {
    	console.log('Test Function');
    },
});