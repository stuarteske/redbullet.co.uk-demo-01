Ext.define('rbdemo.controller.Activity', {
    extend: 'Ext.app.Controller',
    
    requires: [
    	'rbdemo.view.ActivityContainingPanel',
    	'rbdemo.view.EventsViewEventPanel',
    ],
    
    // Launch
    launch: function(app) {
        this.callParent(arguments);
        //console.log("Activity Controller Launched");
    },
    
    // Init
	init: function () {
		this.callParent(arguments);
		//console.log("Controller Inited");
	},
    
    config: {
        refs: {
        	// Activity Navigation View
        	navigationView: 'xtypeActivityNavicationView',
        	navigationViewFilterBtn: 'xtypeActivityNavicationView #itemIdNavigationFilterActivityBtn',
        	// Notification List
        	navigationList: 'xtypeActivityContainingPanel #itemIdNotificationList',
        	timelineList: 'xtypeActivityContainingPanel #itemIdTimelineList',
        },
        control: {
        	// Activity Navigation View
        	navigationView: {
        		activeitemchange: 'onNavigationViewActiveItemChange'
        	},
        	// Notification List
        	navigationList: {
        		itemtap: 'onNotificationListItemTap'
        	},
        	timelineList: {
        		itemtap: 'onTimelineListItemTap'
        	},
        }
    },
    
    // Commands.
    onNavigationViewActiveItemChange: function( container, value, oldValue, eOpts) {
    	switch (value.xtype) {
    		case 'xtypeEventsViewEventPanel':
    			this.getNavigationViewFilterBtn().setHidden(true);
    			break;
    		case 'xtypeActivityStatisticsViewPanel':
    			this.getNavigationViewFilterBtn().setHidden(true);
    			break;
    		case 'xtypeActivityAchievementViewPanel':
    			this.getNavigationViewFilterBtn().setHidden(true);
    			break;
    		case 'ActivityAchievementTwitterViewPanel':
    			this.getNavigationViewFilterBtn().setHidden(true);
    			break;
    		case 'ActivityAchievementFacebookViewPanel':
    			this.getNavigationViewFilterBtn().setHidden(true);
    			break;
    		default:
    			this.getNavigationViewFilterBtn().setHidden(false);
    			break;
    	}
    },
    onNotificationListItemTap: function(dataview, index, target, record, e, options) {
    	//var controller = this.getApplication().getController('Activity');
    	var controller = this;
    	//console.log(controller.getNavigationView());
    	switch ( index ) {
    		case 0:
    			controller.getNavigationView().push({xtype:'xtypeEventsViewEventPanel'});
    			break;
    		case 1:
    			controller.getNavigationView().push({xtype:'xtypeActivityStatisticsViewPanel'});
    			break;
    		case 2:
    			controller.getNavigationView().push({xtype:'xtypeActivityAchievementViewPanel'});
    			break;
    		case 3:
    			controller.getNavigationView().push({xtype:'xtypeActivityAchievementTwitterViewPanel'});
    			break;
    		case 4:
    			controller.getNavigationView().push({xtype:'xtypeActivityAchievementFacebookViewPanel'});
    			break;
    		default:
    			break;
    	}
    },
    onTimelineListItemTap:  function(dataview, index, target, record, e, options) {
    	var controller = this;
    	//console.log(controller.getNavigationView());
    	if (index == 1) {
    		controller.getNavigationView().push({xtype:'xtypeEventsViewEventPanel'});
    	}
    },
    testFunction: function() {
    	console.log('Test Function');
    },
});