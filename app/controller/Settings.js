Ext.define('rbdemo.controller.Settings', {
    extend: 'Ext.app.Controller',
    
    requires: [
    ],
    
    // Launch
    launch: function(app) {
        this.callParent(arguments);
        //console.log("Settings Controller Launched");
    },
    
    // Init
	init: function () {
		this.callParent(arguments);
		//console.log("Controller Inited");
	},
    
    config: {
        refs: {
        	// Settings Navigation View
        	navigationView: 'xtypeSettingsMainNavicationView',
        	navigationViewLogoutBtn: 'xtypeSettingsMainNavicationView #logoutBtn',
        	navigationViewProfileList: 'xtypeSettingsMainNavicationView list',
        	// Add service
        	settingsAddServiceCreateBtn: 'xtypeSettingsAddService button[ui=confirm]',
        	settingsAddServiceAddPhotoBtn: 'xtypeSettingsAddService button[ui=normal]',
        	// Facebook Settings
        	settingsFacebookSettingsVerifyBtn: 'xtypeSettingsSocialToolsFacebookPanel button[ui=confirm]',
        	// Twitter Settings
        	settingsTwitterSettingsVerifyBtn: 'xtypeSettingsSocialToolsTwitterPanel button[ui=confirm]',
        },
        control: {
        	// Settings Navigation View
        	navigationView: {
        		activeitemchange: 'onNavigationViewActiveItemChange',
        	},
        	navigationViewLogoutBtn: {
        		tap: 'onTapNavigationViewLogoutBtn',
        	},
        	navigationViewProfileList: {
        		itemtap: 'onItemTapNavigationViewProfileList',
        	},
        	// Add service
        	settingsAddServiceCreateBtn: {
        		tap: 'onTapSettingsAddServiceCreateBtn',
        	},
        	settingsAddServiceAddPhotoBtn: {
        		tap: 'onTapSettingsAddServiceAddPhotoBtn',
        	},
        	// Facebook Settings
        	settingsFacebookSettingsVerifyBtn: {
        		tap: 'onTapSettingsFacebookSettingsVerifyBtn',
        	},
        	// Twitter Settings
        	settingsTwitterSettingsVerifyBtn: {
        		tap: 'onTapSettingsTwitterSettingsVerifyBtn',
        	}
        }
    },
    
    // Commands.
    onNavigationViewActiveItemChange: function(container, value, oldValue, eOpts) {
    	switch (value.xtype) {
    		case 'xtypeSettingsYourProfile':
    			this.getNavigationViewLogoutBtn().setHidden(true);
    			break;
    		case 'xtypeSettingsAddService':
    			this.getNavigationViewLogoutBtn().setHidden(true);
    			break;
    		case 'xtypeSettingsSocialToolsFacebookPanel':
    			this.getNavigationViewLogoutBtn().setHidden(true);
    			break;
    		case 'xtypeSettingsSocialToolsTwitterPanel':
    			this.getNavigationViewLogoutBtn().setHidden(true);
    			break;
    		case 'xtypeSettingsViewService':
    			this.getNavigationViewLogoutBtn().setHidden(true);
    			break;
    		case 'xtypeSettingsViewServiceSprayTan':
    			this.getNavigationViewLogoutBtn().setHidden(true);
    			break;
    		default:
    			this.getNavigationViewLogoutBtn().setHidden(false);
    			break;
    	}
    },
    onTapNavigationViewLogoutBtn: function (button, e, eOpts) {
    	Ext.Msg.confirm('Logout', 'Are you sure that you would like to logout?', function(e) {
			if (e == 'yes') {
				// Confirmed
				//controller.getNavigationView().pop();
			} else {
				// Denied
				//controller.testFunction();
			}
		});
    },
    onItemTapNavigationViewProfileList: function(dataview, index, target, record, e, options) {
    	switch (index) {
    		case 0:
    			this.getNavigationView().push({xtype:'xtypeSettingsYourProfile'});
    			break;
    		case 1:
    			this.getNavigationView().push({xtype:'xtypeSettingsAddService'});
    			break;
    		case 2:
    			this.getNavigationView().push({xtype:'xtypeSettingsSocialToolsFacebookPanel'});
    			break;
    		case 3:
    			this.getNavigationView().push({xtype:'xtypeSettingsSocialToolsTwitterPanel'});
    			break;
    		case 4:
    			this.getNavigationView().push({xtype:'xtypeSettingsViewService'});
    			break;
    		case 5:
    			this.getNavigationView().push({xtype:'xtypeSettingsViewServiceSprayTan'});
    			break;
    		default:
    			break;
    	}
    },
    // Add service
	onTapSettingsAddServiceCreateBtn: function (button, e, eOpts) {
		this.getNavigationView().pop();
	},
	onTapSettingsAddServiceAddPhotoBtn: function (button, e, eOpts) {
		Ext.Msg.alert('Add Photo', 'This allow the user to add a photo to the service.', function(e) {});
	},
	// Facebook Settings
	onTapSettingsFacebookSettingsVerifyBtn: function (button, e, eOpts) {
		this.getNavigationView().pop();
	},
	// Twitter Settings
    onTapSettingsTwitterSettingsVerifyBtn: function (button, e, eOpts) {
    	this.getNavigationView().pop();
    },
    testFunction: function() {
    	console.log('Test Function');
    },
});