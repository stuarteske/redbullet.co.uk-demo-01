Ext.define('rbdemo.model.AchievementBookingsThisWeek', {
    extend: 'Ext.data.Model',
    
    config: {
        fields: ['service', 'icon', 'appointment', 'group']
    }
});