Ext.define('rbdemo.model.Settings', {
    extend: 'Ext.data.Model',
    
    config: {
        fields: ['title', 'group', 'weight']
    }
});