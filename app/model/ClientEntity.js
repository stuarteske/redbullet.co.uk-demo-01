Ext.define('rbdemo.model.ClientEntity', {
    extend: 'Ext.data.Model',
    
    config: {
        fields: ['firstname', 'surname', 'icon', 'appointment']
    }
});