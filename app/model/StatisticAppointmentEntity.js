Ext.define('rbdemo.model.StatisticAppointmentEntity', {
    extend: 'Ext.data.Model',
    
    config: {
        fields: ['service', 'icon', 'appointment', 'group']
    }
});