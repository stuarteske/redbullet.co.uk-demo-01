Ext.define('rbdemo.model.ActivityTimelineEntity', {
    extend: 'Ext.data.Model',
    
    config: {
        fields: [
        	'notification_time',
        	'notification_icon',
        	'notification_type',
        	'notification_description',
        	'notification_spec'
        ]
    }
});