Ext.define('rbdemo.model.EventDate', {
    extend: 'Ext.data.Model',
    
    config: {
        fields: ['day', 'month', 'bookings', 'image']
    }
});