Ext.define("rbdemo.view.SettingsSocialToolsFacebookPanel", {
    extend: 'Ext.form.Panel',
    requires: [
    	'Ext.form.Panel',
    	'Ext.field.Select',
    	'Ext.field.DatePicker',
    	'Ext.field.Toggle',
    	'Ext.field.Email',
    	'Ext.field.Number',
    	'Ext.field.Password'
    ],
    
    xtype: 'xtypeSettingsSocialToolsFacebookPanel',
    
    config: {
    	title: 'Facebook Settings',
    	items: [
    		{
    			xtpe: 'fieldset',
    			title: 'User',
    			items: [
    				{
    					xtype: 'textfield',
						label: 'Username',
						name: 'username',
					},
					{
    					xtype: 'passwordfield',
						label: 'Password',
						name: 'password',
					},
    			],
    		},
    		{
    			xtype:'fieldset',
    			title: 'Settings',
    			items: [
    				{
						xtype: 'checkboxfield',
						name : 'ncc',
						label: 'Post Achievements?',
						value: 'ncc',
						checked: false,
						labelWidth:'70%',
					},
					{
						xtype: 'checkboxfield',
						name : 'nuc',
						label: 'Post New Services',
						value: 'nuc',
						checked: false,
						labelWidth:'70%',
					},
    			],
    		},
    		{
    			xtype:	'fieldset',
    			items: [
    				{
						xtype:	'button',
						text:	'Save and Verify',
						ui:		'confirm',
					}
    			],
    		},
    	]
    }
});