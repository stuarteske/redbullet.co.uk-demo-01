Ext.define("rbdemo.view.EventsInitialPanel", {
    extend: 'Ext.Panel',
    requires: [
    	'Ext.Panel',
    	'Ext.List',
    	'Ext.data.Store'
    ],
    
    xtype: 'xtypeEventsInitialPanel',
    
    config: {
    	title: 'Your Events',
    	layout:'card',
    	items: [
    		{
				xtype: 'list',
				styleHtmlContent:true,
				styleHtmlCls:'appointmentlist',
				onItemDisclosure: true,
				itemTpl: '<div><span style="font-weight:bold;font-size:0.8em;">{day}</span><div class="indicator"><span class="freetime"><img src="{image}" width="26" height="26" /></span><span class="badgenumber">{bookings}</span></div></div>',
   				store: 'EventSummaryDataStore',
   				grouped: true,
				items: [
					{
						xtype: 'toolbar',
						cls: 'toolbarthin',
						docked: 'top',
						height: 40,
						
						items: [
							{ xtype: 'spacer' },
							{
								xtype: 'searchfield',
								placeHolder: 'Search...',
								cls:'toolbar-search-field',
								listeners: {
									scope: this,
									//clearicontap: this.onSearchClearIconTap,
									//keyup: this.onSearchKeyUp
								}
							},
							{ xtype: 'spacer' }
						]
					}
				],
				listeners: {
				   show: {
						fn: function(list, eOpts) {
							//console.log('List Show Fired');
							//var listStore = list.getStore();
							//listStore.sort('nday', 'ASC');
							//list.refresh();
							
							//var today = new Date()
							//console.log(today);
							
							var scroller = list.getScrollable().getScroller();
							scroller.scrollTo(0, 533);
					   }
				   }
			   },
    		}
    	]
    }
});