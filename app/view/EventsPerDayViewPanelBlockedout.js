Ext.define("rbdemo.view.EventsPerDayViewPanelBlockedout", {
    extend: 'Ext.Panel',
    requires: [
    	'Ext.Panel',
    	'Ext.dataview.List',
    	'Ext.data.Store'
    ],
    
    xtype: 'xtypeEventsPerDayViewPanelBlockedout',
    
    config: {
    	title: 'Your Events',
    	layout:'card',
    	items: [
    		{
				xtype: 'list',
				styleHtmlContent:true,
				styleHtmlCls:'listclass',
				emptyText: "Check your internet connection and try again.",
				loadingText: 'Loading...',
				baseCls: 'events-per-day-list',
				onItemDisclosure: true,
				itemTpl: ['<div class="itemimagecontainer"><img src="{icon}" /></div><div style="margin-left:60px;"><h6 class="item-header">{type}</h6><p class="item-body"><span class="item-emphasis">{emphasis}</span> {description}</p><div style="position:absolute;float:left;left:0px;margin-top:17px;width:100%;height:18px;background:url(resources/images/list_divider.png) transparent top center no-repeat;"></div></div>'],
				data: [
					{ type: 'Blocked Out - All Day',  emphasis: 'Trade Show', description: 'in London', icon: 'resources/icons/star.png' },
				],
   				grouped: false,
				items: [
					{
						xtype: 'toolbar',
						cls: 'toolbarthinwithtitle',
						docked: 'top',
						height: 40,
						title:'MON 6 AUG',
						layout: {
							pack: 'justify',
							align: 'center',
						},
						items: [
							{ 
								xtype: 'button',
								cls: 'toolbar-prev-btn',
								text:'<',
								align:'left',
							},
							{
								xtype: 'spacer'
							},
							{ 
								xtype: 'button',
								cls: 'toolbar-next-btn',
								text:'>',
								align:'right',
							}
						]
					}
				],
    		}
    	]
    }
});