Ext.define("rbdemo.view.EventsCreateEventFormPanel", {
    extend: 'Ext.form.Panel',
    requires: [
    	'Ext.form.Panel',
    	'Ext.field.Select',
    	'Ext.field.DatePicker',
    	'Ext.field.Toggle'
    ],
    
    xtype: 'xtypeEventsCreateEventFormPanel',
    
    config: {
    	title: 'Add Event',
    	items: [
    		{
    			xtype:'fieldset',
    			title: 'Client',
    			items: [
    				{
    					xtype: 'selectfield',
						label: 'Client',
						name: 'client',
						options: [
							{text: 'George Ali',  value: '1'},
							{text: 'Carr Aldred', value: '2'},
							{text: 'Ryan Ashley',  value: '3'},
							{text: 'Yasemin Balshaw',  value: '4'},
							{text: 'Nathan Bamber',  value: '5'},
							{text: 'Hugo Baskin',  value: '6'}
						]
					},
					{
						xtype: 'button',
						text: 'Create Client',
						ui:'normal',
					},
				],
    		},
    		{
    			xtype:'fieldset',
    			title: 'Details',
				items: [
					{
    					xtype: 'selectfield',
						label: 'Service',
						name: 'service',
						options: [
							{text: 'Spray Tan',  value: '1'},
							{text: 'Cut & Blow Dry', value: '2'},
							{text: 'Highlights', value: '3'},
							{text: 'Manicure', value: '4'},
						]
					},
					{
						xtype: 'datepickerfield',
						label: 'Date',
						name: 'date',
						value: new Date()
					},
					{
						xtype: 'selectfield',
						label: 'Time',
						name: 'time',
						options: [
							{text: '07:00', value: '1'},
							{text: '08:00',  value: '2'},
							{text: '09:00',  value: '3'},
							{text: '10:00', value: '4'},
							{text: '11:00',  value: '5'},
							{text: '12:00',  value: '6'},
							{text: '13:00', value: '7'},
							{text: '14:00',  value: '8'},
							{text: '15:00',  value: '9'},
							{text: '16:00', value: '10'},
							{text: '17:00',  value: '11'},
							{text: '19:00',  value: '12'},
							{text: '20:00', value: '13'},
							{text: '21:00',  value: '14'},
							{text: '22:00',  value: '15'},
						]
					},
					{
						xtype: 'selectfield',
						label: 'Duration',
						name: 'duration',
						options: [
							{text: '00:15', value: '1'},
							{text: '00:30',  value: '2'},
							{text: '00:45',  value: '3'},
							{text: '01:00', value: '4'},
							{text: '01:15',  value: '5'},
							{text: '01:30',  value: '6'},
							{text: '01:45', value: '7'},
							{text: '02:00',  value: '8'},
							{text: '02:15',  value: '9'},
							{text: '02:30', value: '10'},
							{text: '02:45',  value: '11'},
							{text: '03:00',  value: '12'},
							{text: '03:15', value: '13'},
							{text: '03:30',  value: '14'},
							{text: '03:45',  value: '15'},
						]
					},
					{
						xtype: 'textfield',
						label: 'Availability',
						name: 'avalibility',
						disabled:true,
						value:'Available (Indicator)',
					},
					{
						xtype: 'textareafield',
						label: 'Notes',
						maxRows: 4,
						name: 'notes',
						placeHolder:'Add your notes here.',
					},
				],
    		},
    		{
    			xtype:'fieldset',
    			title: 'Notify',
    			instructions: 'If this event requires confirmation? Please select the notification channels.',
    			items: [
    				{
    					xtype: 'togglefield',
						name: 'requires_confirmation',
						label: 'Requires Confirmation',
						labelWidth:'60%',
						value: 1,
    				},
    				{
    					xtype: 'togglefield',
						name: 'email_notification',
						label: 'Via Email',
						labelWidth:'60%',
						value: 1,
    				},
    				{
    					xtype: 'togglefield',
						name: 'sms_notification',
						label: 'Via SMS',
						labelWidth:'60%',
						value: 0,
    				},
    				{
    					xtype: 'togglefield',
						name: 'push_notification',
						label: 'Via Push',
						labelWidth:'60%',
						value: 0,
    				},
    			]
    		},
    		/*{
    			xtype:'fieldset',
    			title: 'Event',
    			items: [
    				{
    					xtype: 'selectfield',
						label: 'Event',
						name: 'event',
						options: [
							{text: 'New Appointment',  value: '1'},
						]
					},
					{
    					xtype: 'selectfield',
						label: 'Status',
						name: 'status',
						options: [
							{text: 'Unconfirmed',  value: '1'},
							{text: 'Confirmed',  value: '2'}
						]
					},
					{
						xtype: 'datepickerfield',
						label: 'Date',
						name: 'date',
						value: new Date()
					},
					{
						xtype: 'selectfield',
						label: 'Time',
						name: 'time',
						options: [
							{text: '07:00', value: '1'},
							{text: '08:00',  value: '2'},
							{text: '09:00',  value: '3'},
							{text: '10:00', value: '4'},
							{text: '11:00',  value: '5'},
							{text: '12:00',  value: '6'},
							{text: '13:00', value: '7'},
							{text: '14:00',  value: '8'},
							{text: '15:00',  value: '9'},
							{text: '16:00', value: '10'},
							{text: '17:00',  value: '11'},
							{text: '19:00',  value: '12'},
							{text: '20:00', value: '13'},
							{text: '21:00',  value: '14'},
							{text: '22:00',  value: '15'},
						]
					},
					{
						xtype: 'selectfield',
						label: 'Duration',
						name: 'duration',
						options: [
							{text: '00:15', value: '1'},
							{text: '00:30',  value: '2'},
							{text: '00:45',  value: '3'},
							{text: '01:00', value: '4'},
							{text: '01:15',  value: '5'},
							{text: '01:30',  value: '6'},
							{text: '01:45', value: '7'},
							{text: '02:00',  value: '8'},
							{text: '02:15',  value: '9'},
							{text: '02:30', value: '10'},
							{text: '02:45',  value: '11'},
							{text: '03:00',  value: '12'},
							{text: '03:15', value: '13'},
							{text: '03:30',  value: '14'},
							{text: '03:45',  value: '15'},
						]
					},
					{
						xtype: 'textfield',
						label: 'Availability',
						name: 'avalibility',
						disabled:true,
						value:'Available',
					},
    			],
    		},*/
    		/*{
    			xtype:'fieldset',
    			title: 'Detail',
    			items: [
    				{
    					xtype: 'selectfield',
						label: 'Client',
						name: 'client',
						options: [
							{text: 'George Ali',  value: '1'},
							{text: 'Carr Aldred', value: '2'},
							{text: 'Ryan Ashley',  value: '3'},
							{text: 'Yasemin Balshaw',  value: '4'},
							{text: 'Nathan Bamber',  value: '5'},
							{text: 'Hugo Baskin',  value: '6'}
						]
					},
					{
    					xtype: 'selectfield',
						label: 'Organisation',
						name: 'organisation',
						options: [
							{text: 'Uppercuts',  value: '1'},
							{text: 'Freelance', value: '2'},
						]
					},
					{
						xtype: 'selectfield',
						label: 'Technician',
						name: 'technician',
						options: [
							{text: 'You',  value: '1'},
							{text: 'Tom Bayton', value: '2'},
							{text: 'Hugo Baskin', value: '3'},
						]
					},
					{
    					xtype: 'selectfield',
						label: 'Product',
						name: 'product',
						options: [
							{text: 'Spray Tan',  value: '1'},
							{text: 'Cut & Blow Dry', value: '2'},
							{text: 'Highlights', value: '3'},
							{text: 'Manicure', value: '4'},
						]
					},
    			],
    		},*/
    		{
    			xtype:'fieldset',
    			items: [
    				{
						xtype: 'button',
						text: 'Create',
						ui:'confirm',
					}
    			],
    		}
    	]
    }
});