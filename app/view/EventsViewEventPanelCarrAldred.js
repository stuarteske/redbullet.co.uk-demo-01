Ext.define("rbdemo.view.EventsViewEventPanelCarrAldred", {
    extend: 'Ext.Panel',
    requires: [
    	'Ext.Panel',
    ],
    
    xtype: 'xtypeEventsViewEventPanelCarrAldred',
    
    config: {
    	title: 'View Event',
    	layout:'vbox',
    	scrollable: true,
    	items: [
    		{
    			xtype: 'panel',
    			height:10,
    		},
    		{
    			xtype: 'panel',
    			html:'<div><div style="margin:0px 10px;border:solid 1px #ccc;padding:10px;"><span style="font-weight:bold;font-size:0.75em;">Status:</span> <span style="font-weight:normal;font-size:0.75em;">In Progress</span><div style="float:right;"><img src="resources/images/dropout.png" /></div></div></div>',
    			listeners: {
					tap: {
						fn: function() {
							//console.log('Panel Clicked')
							rbdemo.globals['key3'].show();
						},
						element: 'element'
					},
				}
    		},
    		{
    			xtype: 'panel',
    			height:10,
    		},
    		{
    			xtype: 'panel',
    			html:'<div><div style="margin:0px 10px;border:solid 1px #ccc;padding:10px;"><div style="float:left;margin-right:10px;margin-top:-0.1em;"><img style="opacity:0.75;" src="resources/icons/guest.png" width="48" height="48" /></div><span style="font-weight:bold;font-size:0.85em;">Carr Aldred</span><br /><span style="font-weight:normal;font-size:0.7em;color:#666;">Your Customer</span><div style="float:right;margin-top:-0.65em;"><img src="resources/images/disclosure.png" /></div></div></div>',
    			listeners: {
					tap: {
						fn: function() {
							console.log('Panel Clicked')
						},
						element: 'element'
					},
				}
    		},
    		{
    			xtype: 'panel',
    			height:10,
    		},
    		{
    			xtype: 'panel',
    			html:'<div><div style="margin:0px 10px;border:solid 1px #ccc;padding:10px;"><div style="float:left;margin-right:18px;margin-top:0.35em;margin-left:9px;"><img src="resources/icons/bookings.png" width="32" height="32" /></div><span style="font-weight:normal;font-size:0.7em;color:#666;">Studio / Appointment</span><br /><span style="font-weight:bold;font-size:0.75em;">Spray Tan</span> <span style="font-weight:normal;font-size:0.75em;color:#666;">with</span> <span style="font-weight:bold;font-size:0.75em;">You</span></div></div>',
    		},
    		{
    			xtype: 'panel',
    			height:10,
    		},
    		{
    			xtype: 'panel',
    			html:'<div><div style="margin:0px 10px;border:solid 1px #ccc;padding:10px;"><div style="float:left;margin-right:18px;margin-top:0.35em;margin-left:9px;"><img src="resources/icons/blank.png" width="32" height="32" /></div><span style="font-weight:normal;font-size:0.7em;color:#666;">Fri 02 Nov 2012</span><br /><span style="font-weight:bold;font-size:0.75em;">11:00 - 12:00</span></div></div>',
    		},
    		{
    			xtype: 'panel',
    			height:10,
    		},
    		{
    			xtype: 'panel',
    			html:'<div><div style="margin:0px 10px;border:solid 1px #ccc;padding:10px;"><p style="font-weight:normal;font-size:0.7em;color:#666;">Booked on &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span style="color:#171717">12/12/12</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; at <span style="color:#171717">12:00</span></p><p style="font-weight:normal;font-size:0.7em;color:#666;">Booked by &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span style="color:#171717">John</span></p><p style="font-weight:normal;font-size:0.7em;color:#666;">Confirmed &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span style="color:#171717">14/12/12</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; by <span style="color:#171717">SMS</span></p></div></div>',
    		},
    		{
    			xtype: 'panel',
    			height:10,
    		},
    	]
    }
});