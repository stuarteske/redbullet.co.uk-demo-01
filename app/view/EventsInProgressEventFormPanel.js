Ext.define("rbdemo.view.EventsInProgressEventFormPanel", {
    extend: 'Ext.form.Panel',
    requires: [
    	'Ext.form.Panel',
    	'Ext.field.Select',
    	'Ext.field.DatePicker',
    	'Ext.field.Toggle'
    ],
    
    xtype: 'xtypeEventsInProgressEventFormPanel',
    
    config: {
    	title: 'Event In Progress',
    	items: [
    		{
    			xtype:'fieldset',
    			items: [
    				{
						xtype: 'button',
						text: 'Started',
						ui:'confirm',
					}
    			],
    		}
    	]
    }
});