Ext.define("rbdemo.view.ClientsInitialPanel", {
    extend: 'Ext.Panel',
    requires: [
    	'Ext.Panel',
    	'Ext.dataview.List',
    	'Ext.data.Store',
    	'Ext.field.Search',
    	'rbdemo.store.ClientData'
    ],
    
    xtype: 'xtypeClientsInitialPanel',
    
    config: {
    	title: 'Your Clients',
    	fullscreen: true,
    	layout:'card',
    	items: [
    		{
				xtype: 'list',
				indexBar: true,
				styleHtmlContent:true,
				styleHtmlCls:'clientlist',
				onItemDisclosure: true,
				itemTpl: '<div class="itemimagecontainer"><img src="{icon}" /></div><div class="contact" style="font-size:0.8em;line-height:1.1em;"><span style="font-weight:normal;">{firstname}</span> <span style="font-weight:bold;">{surname}</span><br /><span style="font-size:0.6em;color:#666;">{appointment}</span></div>',
   				store: 'ClientDataStore',
   				grouped: true,
				items: [
					{
						xtype: 'toolbar',
						docked: 'top',
						cls: 'toolbarthin',
						height: 40,
						
						items: [
							{ xtype: 'spacer' },
							{
								xtype: 'searchfield',
								placeHolder: 'Search...',
								cls:'toolbar-search-field',
								listeners: {
									scope: this,
									//clearicontap: this.onSearchClearIconTap,
									//keyup: this.onSearchKeyUp
								}
							},
							{ xtype: 'spacer' }
						]
					}
				],
    		}
    	]
    }
});