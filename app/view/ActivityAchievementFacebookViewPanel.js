Ext.define("rbdemo.view.ActivityAchievementFacebookViewPanel", {
    extend: 'Ext.Panel',
    requires: [
    	'Ext.Panel',
    ],
    
    xtype: 'xtypeActivityAchievementFacebookViewPanel',
    
    config: {
    	title: 'Facebook',
    	fullscreen: true,
    	scrollable: false,
    	layout:'vbox',
    	items: [
    		{
				xtype: 'panel',
				height: '10px',
			},
			{
				xtype: 'panel',
				html:'<div><div style="margin:0px 10px;border:solid 1px #ccc;padding:6px 10px;"><span style="font-weight:bold;font-size:0.8em;">Facebook</span> <span style="font-weight:normal;font-size:0.8em;"></span><br /><span style="font-weight:normal;font-size:0.7em;color:#171717;">5 Friends</span> <span style="font-weight:normal;font-size:0.7em;color:#666;">added this week.</span> <span style="font-weight:normal;font-size:0.7em;color:#171717;"></span><div style="float:right;margin-top:-0.65em;"></div></div></div>',
				listeners: {
					tap: {
						fn: function() {
							this.up('xtypeClientsMainNavicationView').push({xtype:'xtypeEventsViewEventPanelCarrAldred'});
						},
						element: 'element'
					},
				}
			},
			{
				xtype: 'panel',
				height: '10px',
			},
			{
				xtype: 'list',
				flex:1,
				styleHtmlContent:true,
				styleHtmlCls:'clientlist',
				onItemDisclosure: true,
				itemTpl: '<div class="itemimagecontainer"><img src="{icon}" /></div><div class="contact" style="font-size:0.8em;line-height:1.1em;"><span style="font-weight:normal;">{name}</span> <span style="font-weight:bold;"></span><br /><span style="font-size:0.6em;color:#666;">{notice}</span></div>',
   				data: [
   					{name:'Yasemin', icon:'resources/icons/star.png', notice:'Added on Facebook'},
   					{name:'Louis', icon:'resources/icons/star.png', notice:'Added on Facebook'},
   					{name:'John', icon:'resources/icons/star.png', notice:'Added on Facebook'},
   					{name:'Nathan', icon:'resources/icons/star.png', notice:'Added on Facebook'},
   					{name:'Fatima', icon:'resources/icons/star.png', notice:'Added on Facebook'},
   				],
			},
			{
				xtype: 'panel',
				height: '10px',
			},
    	]
    }
});