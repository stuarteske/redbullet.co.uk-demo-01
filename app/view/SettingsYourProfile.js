Ext.define("rbdemo.view.SettingsYourProfile", {
    extend: 'Ext.Panel',
    requires: [
    	'Ext.Panel',
    ],
    
    xtype: 'xtypeSettingsYourProfile',
    
    initialize: function () {
    	this.callParent(arguments);
    	
    	/*var clientToolbar = {
			xtype : 'toolbar',
			cls:'toolbar-grey',
			docked: 'top',
			height: 40,
			layout:{
				pack: 'center'
			},
			items: [
				{
					xtype: 'segmentedbutton',
					allowMultiple: false,
					height: 22,
					allowDepress:false,
					items: [
						{
							text: 'Profile',
							id:'',
							pressed: true,
							cls: 'toolbarminwidth',
						},
						{
							text: 'Appointments',
							id: '',
							cls: 'toolbarminwidth',
						},
						{
							text: 'Notes',
							id: '',
							cls: 'toolbarminwidth',
						},
						{
							text: 'Activity',
							id: '',
							cls: 'toolbarminwidth clienttoolbarlastitem',
						},
					],
					listeners: {
						toggle: function(container, button, pressed){
							//console.log(navigationView.getActiveItem());
							switch (button.getText()) {
								case 'Profile':
									//console.log('Notification');
									navigationView.setActiveItem(0);
									break;
								case 'Appointments':
									//console.log('Notification');
									navigationView.setActiveItem(1);
									break;
								case 'Activity':
									//console.log('Notification');
									navigationView.setActiveItem(3);
									break;
								case 'Notes':
									//console.log('Notification');
									navigationView.setActiveItem(2);
									break;
								default:
									break;
							}
						}
					}
				},
			]
		}*/
    
    	/*var navigationView = Ext.create('Ext.NavigationView', {
			//xtype: 'navigationview',
			id: 'idClientDetailsNavigationContainer',
			flex:1,
			navigationBar: {
				hidden: true,
			},
		});*/
		
		var clientProfilePanel = Ext.create('Ext.Panel', {
			layout:'vbox',
			scrollable:true,
			items: [
				/*{
					xtype:'panel',
					style:'background-color:#eee;',
					html:'<div><div style="margin:10px;padding:6px 0px;"><!--<div style="fload:left;margin-right:10px;"><img src="resources/icons/meeting.png" width="16" height="16" /></div>--><p style="font-size:0.64em;cont-weight:normal;color:#666;padding:0px 4px;"><img style="float:left;margin-right:6px;" src="resources/icons/meeting.png" width="14" height="14" />2 weeks until Carr Aldred\'s birthday!</p></div></div>',
				},*/
				{
					xtype:'panel',
					height:'10px',
				},
				{
					xtype: 'panel',
					html:'<div><div style="margin:0px 10px;border:solid 1px #ccc;padding:10px;"><div style="float:left;margin-right:10px;margin-top:-0.1em;"><img style="opacity:0.75;border:solid #ccc 1px;" src="resources/icons/personplus.png" width="48" height="48" /></div><span style="font-weight:bold;font-size:0.85em;">Technician Name</span><br /><span style="font-weight:normal;font-size:0.7em;color:#666;">Your Profile</span><div style="float:right;margin-top:-0.65em;"><img src="resources/images/dropout.png" /></div></div></div>',
					listeners: {
						tap: {
							fn: function() {
								Ext.Msg.alert('Add Photo', 'Touching this panel will allow you to add a photo of yourself.', function(e) {});
							},
							element: 'element'
						},
					}
				},
				{
					xtype: 'panel',
					html:'<div><div style="margin:0px 10px;border:solid 1px #ccc;border-top:0px;padding:10px;"><div style="font-weight:normal;font-size:0.65em;color:#666;min-width:70px;float:left;">Daytime</div><div style="font-weight:normal;font-size:0.65em;color:#171717;">07047 972 634<div style="float:right;margin-top:-0.45em;"><img src="resources/images/disclosure.png" /></div></div></div></div>',
					listeners: {
						tap: {
							fn: function() {
								Ext.Msg.alert('Phone Client', 'Touching this will phone the daytime number.', function(e) {});
							},
							element: 'element'
						},
					}
				},
				{
					xtype: 'panel',
					html:'<div><div style="margin:0px 10px;border:solid 1px #ccc;border-top:0px;padding:10px;"><div style="font-weight:normal;font-size:0.65em;color:#666;min-width:70px;float:left;">Evening</div><div style="font-weight:normal;font-size:0.65em;color:#171717;">07047 972 634<div style="float:right;margin-top:-0.45em;"><img src="resources/images/disclosure.png" /></div></div></div></div>',
					listeners: {
						tap: {
							fn: function() {
								Ext.Msg.alert('Phone Client', 'Touching this will phone the evening number.', function(e) {});
							},
							element: 'element'
						},
					}
				},
				{
					xtype: 'panel',
					html:'<div><div style="margin:0px 10px;border:solid 1px #ccc;border-top:0px;padding:10px;"><div style="font-weight:normal;font-size:0.65em;color:#666;min-width:70px;float:left;">Email</div><div style="font-weight:normal;font-size:0.65em;color:#171717;">technician@example.com<div style="float:right;margin-top:-0.45em;"><img src="resources/images/disclosure.png" /></div></div></div></div>',
					listeners: {
						tap: {
							fn: function() {
								Ext.Msg.alert('Email', 'Touching this will email the supplied address', function(e) {});
							},
							element: 'element'
						},
					}
				},
				{
					xtype: 'panel',
					html:'<div><div style="margin:0px 10px;border:solid 1px #ccc;border-top:0px;padding:10px;"><div style="font-weight:normal;font-size:0.65em;color:#666;min-width:70px;float:left;">Address<br />&nbsp<br />&nbsp;</div><div style="font-weight:normal;font-size:0.65em;color:#171717;">12 Upper Street<br />Kent<br />DF5 WS9</div></div></div>',
				},
				{
					xtype: 'panel',
					html:'<div><div style="margin:0px 10px;border:solid 1px #ccc;border-top:0px;padding:10px;"><div style="font-weight:normal;font-size:0.65em;color:#666;min-width:70px;float:left;">Birthday<br />Occupation</div><div style="font-weight:normal;font-size:0.65em;color:#171717;">May 17, 1967 (44 Year Old)<br />Technician</div></div></div>',
				},
				{
					xtype: 'panel',
					height:10,
				},
			]
		});
		
		/*var clientAppointmentsPanel = Ext.create('Ext.Panel', {
			layout: 'vbox',
			scrollable:true,	
			items: [
				{
					xtype: 'panel',
					height: '10px',
				},
				{
					xtype: 'panel',
					html:'<div><div style="margin:0px 10px;border:solid 1px #ccc;padding:4px;background-color:#ddd;border-radius:5px;-moz-border-radius:5px;-webkit-border-radius:5px;"><div style="font-weight:normal;font-size:0.65em;color:#171717;width:100%;text-align:center;">Add Appointment</div></div></div>',
					listeners: {
						tap: {
							fn: function() {
								this.up('xtypeClientsMainNavicationView').push({xtype:'xtypeEventsCreateEventFormPanel'});
							},
							element: 'element'
						},
					}
				},
				{
					xtype: 'panel',
					height: '10px',
				},
				{
					xtype: 'panel',
					html:'<div><div style="margin:0px 10px;border:solid 1px #ccc;padding:6px 10px;"><span style="font-weight:bold;font-size:0.8em;">Mon 14 Sept</span> <span style="font-weight:normal;font-size:0.8em;">@ 10:00</span><br /><span style="font-weight:normal;font-size:0.7em;color:#171717;">Spray Tan</span> <span style="font-weight:normal;font-size:0.7em;color:#666;">with</span> <span style="font-weight:normal;font-size:0.7em;color:#171717;">Millie</span><div style="float:right;margin-top:-0.65em;"><img src="resources/images/disclosure.png" /></div></div></div>',
					listeners: {
						tap: {
							fn: function() {
								this.up('xtypeClientsMainNavicationView').push({xtype:'xtypeEventsViewEventPanelCarrAldred'});
							},
							element: 'element'
						},
					}
				},
				{
					xtype: 'panel',
					height: '10px',
				},
				{
					xtype: 'panel',
					html:'<div><div style="margin:0px 10px;border:solid 1px #ccc;padding:6px 10px;"><span style="font-weight:bold;font-size:0.8em;">Wed 28 July</span> <span style="font-weight:normal;font-size:0.8em;">@ 10:00</span><br /><span style="font-weight:normal;font-size:0.7em;color:#171717;">Spray Tan</span> <span style="font-weight:normal;font-size:0.7em;color:#666;">with</span> <span style="font-weight:normal;font-size:0.7em;color:#171717;">Anna</span><div style="float:right;margin-top:-0.65em;"><img src="resources/images/disclosure.png" /></div></div></div>',
					listeners: {
						tap: {
							fn: function() {
								this.up('xtypeClientsMainNavicationView').push({xtype:'xtypeEventsViewEventPanelCarrAldred'});
							},
							element: 'element'
						},
					}
				},
				{
					xtype: 'panel',
					height: '10px',
				},
				{
					xtype: 'panel',
					html:'<div><div style="margin:0px 10px;border:solid 1px #ccc;padding:6px 10px;background-color:#f7f9f9;"><span style="font-weight:bold;font-size:0.8em;color:#666;">Thursday 2 March</span> <span style="font-weight:normal;font-size:0.8em;color:#666;">@ 16:00</span><br /><span style="font-weight:normal;font-size:0.7em;color:#666;">Spray Tan</span> <span style="font-weight:normal;font-size:0.7em;color:#666;">with</span> <span style="font-weight:normal;font-size:0.7em;color:#666;">Millie</span><div style="float:right;margin-top:-0.65em;"><img src="resources/images/disclosure.png" /></div></div></div>',
					listeners: {
						tap: {
							fn: function() {
								this.up('xtypeClientsMainNavicationView').push({xtype:'xtypeEventsViewEventPanelCarrAldred'});
							},
							element: 'element'
						},
					}
				},
				{
					xtype: 'panel',
					height: '10px',
				},
				{
					xtype: 'panel',
					html:'<div><div style="margin:0px 10px;border:solid 1px #ccc;padding:6px 10px;background-color:#f7f9f9;"><span style="font-weight:bold;font-size:0.8em;color:#666;">Friday 23 Jan</span> <span style="font-weight:normal;font-size:0.8em;color:#666;">@ 12:00</span><br /><span style="font-weight:normal;font-size:0.7em;color:#666;">Spray Tan</span> <span style="font-weight:normal;font-size:0.7em;color:#666;">with</span> <span style="font-weight:normal;font-size:0.7em;color:#666;">Tony</span><div style="float:right;margin-top:-0.65em;"><img src="resources/images/disclosure.png" /></div></div></div>',
					listeners: {
						tap: {
							fn: function() {
								this.up('xtypeClientsMainNavicationView').push({xtype:'xtypeEventsViewEventPanelCarrAldred'});
							},
							element: 'element'
						},
					}
				},
				{
					xtype: 'panel',
					height: '10px',
				},
			],
		});*/
		
		/*var clientActivityPanel = Ext.create('Ext.List', {
			//xtype: 'list',
			styleHtmlContent:true,
			styleHtmlCls:'timeline-list',
			onItemDisclosure: true,
			itemTpl: '<div><p style="font-size:0.6em;color:#666;padding-bottom:2px;"><span style="display:block;float:left;margin-right:4px;width:34px;text-align:center;">{notification_time}</span> <span>{notification_type}</span></p><p><div style="float:left;padding:4px 4px 0px 0px;" ><img src="{notification_icon}" /></div> <span style="font-size:0.8em;">{notification_description}</span></p><p style="font-size:0.6em;padding-top:3px;">{notification_spec}</p></div>',
			store: 'ClientActivityTimelineStore',
			grouped: true
		});*/
		
		/*var clientNotesPanel = Ext.create('Ext.Panel', {
			layout: 'vbox',
			scrollable:true,	
			items: [
				{
					xtype: 'panel',
					height: '10px',
				},
				{
					xtype: 'panel',
					html:'<div><div style="margin:0px 10px;border:solid 1px #ccc;padding:4px;background-color:#ddd;border-radius:5px;-moz-border-radius:5px;-webkit-border-radius:5px;"><div style="font-weight:normal;font-size:0.65em;color:#171717;width:100%;text-align:center;">Add Note</div></div></div>',
					listeners: {
						tap: {
							fn: function() {
								this.up('xtypeClientsMainNavicationView').push({xtype:'xtypeNotesCreateNoteFormPanel'});
							},
							element: 'element'
						},
					}
				},
				{
					xtype: 'panel',
					height: '10px',
				},
				{
					xtype: 'panel',
					id: 'idClientNote',
					html:'<div><div style="margin:0px 10px;border:solid 1px #ccc;padding:6px 10px;"><p style="font-weight:normal;font-size:0.7em;color:#666;">Today &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Note added by You</p><p style="font-weight:normal;font-size:0.7em;color:#171717;">Morbi ac odio sed est tempus imperdiet sit amet ut augue. Vestibulum ac neque leo. Sed mollis scelerisque lacus at commodo. Vivamus nulla risus, egestas non fringilla et, accumsan id justo.</p></div></div>',
					listeners: {
						swipe: {
							fn: function() {
								Ext.Msg.confirm('Delete', 'Are you sure that you would like to delete this note?', function(e) {
									if (e == 'yes') {
										// Confirmed
									} else {
										// Denied
										//controller.testFunction();
									}
								});
							},
							element: 'element'
						},
					}
				},
				{
					xtype: 'panel',
					height: '10px',
				},
				{
					xtype: 'panel',
					itemId: 'itemIdClientNote',
					html:'<div><div style="margin:0px 10px;border:solid 1px #ccc;padding:6px 10px;"><p style="font-weight:normal;font-size:0.7em;color:#666;">Today &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Note added by You</p><p style="font-weight:normal;font-size:0.7em;color:#171717;">Morbi ac odio sed est tempus imperdiet sit amet ut augue. Vestibulum ac neque leo. Sed mollis scelerisque lacus at commodo. Vivamus nulla risus, egestas non fringilla et, accumsan id justo.</p><p><div style="padding-top:10px;"><img style="" src="resources/icons/photo_blank.png" />&nbsp;<img src="resources/icons/photo_blank.png" /></div></p></div></div>',
					listeners: {
						swipe: {
							fn: function() {
								Ext.Msg.confirm('Delete', 'Are you sure that you would like to delete this note?', function(e) {
									if (e == 'yes') {
										// Confirmed
									} else {
										// Denied
										//controller.testFunction();
									}
								});
							},
							element: 'element'
						},
					}
				},
				{
					xtype: 'panel',
					height: '10px',
				},
			],
		});*/
    	
    	//navigationView.push(clientProfilePanel);
    	//navigationView.push(clientAppointmentsPanel);
    	//navigationView.push(clientNotesPanel);
    	//navigationView.push(clientActivityPanel);
		//navigationView.setActiveItem(0);
    	
    	this.add([
        	//clientToolbar,
        	//navigationView
        	clientProfilePanel
        ]);
    },
    
    config: {
    	title: 'Your Profile',
    	layout:'card',
    	items: [
    		
    	]
    }
});