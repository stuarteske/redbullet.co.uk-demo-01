Ext.define("rbdemo.view.ClientsCreateClientFormPanel", {
    extend: 'Ext.form.Panel',
    requires: [
    	'Ext.form.Panel',
    	'Ext.field.Select',
    	'Ext.field.DatePicker',
    	'Ext.field.Toggle',
    	'Ext.field.Email'
    ],
    
    xtype: 'xtypeClientsCreateClientFormPanel',
    
    config: {
    	title: 'Add Client',
    	items: [
    		{
    			xtype:'fieldset',
    			items: [
    				{
    					xtype: 'textfield',
						label: 'Name',
						name: 'name',
						placeHolder: 'Client name',
					},
					{
    					xtype: 'emailfield',
						label: 'Email',
						name: 'email',
						placeHolder: 'client@email.com',
					},
					{
    					xtype: 'textfield',
						label: 'Daytime',
						name: 'daytime',
						placeHolder: '078234 3893209',
					},
					/*{
    					xtype: 'selectfield',
						label: 'Favorite Product',
						name: 'product',
						options: [
							{text: 'Spray Tan',  value: '1'},
							{text: 'Cut & Blow Dry', value: '2'},
							{text: 'Highlights', value: '3'},
							{text: 'Manicure', value: '4'},
						]
					},*/
    			],
    		},
    		{
    			xtype: 'fieldset',
    			title:'',
    			items: [
    				{
    					xtype: 'selectfield',
						label: 'Organisation',
						name: 'organisation',
						options: [
							{text: 'Uppercuts',	value: '1'},
							{text: 'Freelance',	value: '2'},
						]
    				},
    				{
    					xtype: 'selectfield',
						label: 'Technician',
						name: 'technician',
						options: [
							{text: 'You',			value: '1'},
							{text: 'Barry Jones',	value: '2'},
							{text: 'Julie Adams',	value: '3'},
						]
    				},
    				{
    					xtype: 'togglefield',
						name: 'notify',
						label: 'Notify',
						value: 1,
    				},
    			],
    		},
    		{
    			xtype: 'fieldset',
    			title: 'Details',
    			items: [
    				{
    					xtype: 'selectfield',
						label: 'Title',
						name: 'title',
						options: [
							{text: 'Mr',	value: '1'},
							{text: 'Ms',	value: '2'},
							{text: 'Mrs',	value: '3'},
							{text: 'Miss',	value: '4'},
							{text: 'Dr',	value: '5'},
							{text: 'Prof',	value: '6'},
						]
    				},
    				{
						xtype: 'selectfield',
						label: 'Gender',
						name: 'gender',
						options: [
							{text: 'Male',	value: '1'},
							{text: 'Female',value: '2'},
						]
					},
    				{
						xtype: 'datepickerfield',
						label: 'Date',
						name: 'date',
						value: new Date(),
					},
					{
    					xtype: 'textfield',
						label: 'Occupation',
						name: 'occupation',
						placeHolder: '',
					},
    			],
    		},
    		{
    			xtype: 'fieldset',
    			title: 'Contact',
    			items: [
    				{
    					xtype: 'textfield',
						label: 'Evening',
						name: 'evening',
						placeHolder: '078234 3893209',
    				},
    				{
						xtype: 'textareafield',
						label: 'Address',
						maxRows: 5,
						name: 'address',
						placeHolder:'Enter the clients address here.',
					},
    			]
    		},
    		{
    			xtype:'fieldset',
    			items: [
    				{
						xtype: 'button',
						text: 'Client From Address Book',
						ui: 'action',
					}
    			],
    		},
    		{
    			xtype:'fieldset',
    			items: [
    				{
						xtype: 'button',
						text: 'Create',
						ui:'confirm',
					}
    			],
    		},
    	]
    }
});