Ext.define("rbdemo.view.ClientsGroupListingViewPanel", {
    extend: 'Ext.Panel',
    requires: [
    	'Ext.Panel',
    	'Ext.dataview.List',
    	'Ext.data.Store',
    	'Ext.field.Search',
    	'rbdemo.store.ClientData'
    ],
    
    xtype: 'xtypeClientsGroupListingViewPanel',
    
    config: {
    	title: 'Clients',
    	fullscreen: true,
    	layout:'card',
    	items: [
    		{
				xtype: 'list',
				styleHtmlContent:true,
				styleHtmlCls:'clientgrouplist',
				onItemDisclosure: true,
				itemTpl: '<div><span class="listitemicon"><img src="resources/icons/team.png" width="26" height="26" /></span><span style="font-weight:bold;font-size:0.8em;">{name}</span><div class="indicator"><span class="badgenumber">{clients}</span></div></div>',
   				items: [
					{
						xtype: 'toolbar',
						cls: 'toolbarthin',
						docked: 'top',
						height: 40,
						
						items: [
							{ xtype: 'spacer' },
							{
								xtype: 'searchfield',
								placeHolder: 'Search...',
								cls:'toolbar-search-field',
								listeners: {
									scope: this,
									//clearicontap: this.onSearchClearIconTap,
									//keyup: this.onSearchKeyUp
								}
							},
							{ xtype: 'spacer' }
						]
					}
				], 
				data: [
					{ name: 'All Clients', clients: '137', group:'Original Groups' },
					//{ name: 'Your Clients', clients: '47', group:'Original Groups'  },
					//{ name: 'Studio', clients: '124', group:'Original Groups'  },
					//{ name: 'Studio / Your Clients', clients: '37', group:'Original Groups'  },
					//{ name: 'Private', clients: '10', group:'New Groups' },
					{ name: 'Top Clients', clients: '12', group:'New Groups' },
					{ name: 'Best Tippers', clients: '5', group:'New Groups' },
					{ name: 'Recent Activity', clients: '18', group:'New Groups' },
					{ name: 'AWOL', clients: '5', group:'New Groups' },
					
				],
    		}
    	]
    }
});