Ext.define("rbdemo.view.ActivityNavigationView", {
    extend: 'Ext.navigation.View',
    requires: [
    	'Ext.navigation.View',
    	'rbdemo.view.ActivityContainingPanel',
    ],
    
    xtype: 'xtypeActivityNavicationView',
    
    initialize: function () {
    	this.callParent(arguments);
        
        this.add([
        	//titleBar,
        	//twitterFeed
        ]);
	},
    
    config: {
    	iconCls: 'activity',
        navigationBar: {
			cls: 'titlebar-black',
			id: 'idActivityNavigationBar',
			height:40,
			items : [
				{
					xtype:'button',
					text: 'Filter',
					align: 'right',
					cls: 'titlebarbutton',
					itemId: 'itemIdNavigationFilterActivityBtn',
					listeners: {
					   tap: {
							fn: function() {
								rbdemo.globals['key'].show();
							}
					   }
				   }
				},
			],
		},
    	items: [
        	{ xtype: 'xtypeActivityContainingPanel' }
        ]
    }
});