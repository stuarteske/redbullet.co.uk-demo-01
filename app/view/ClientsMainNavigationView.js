Ext.define("rbdemo.view.ClientsMainNavigationView", {
    extend: 'Ext.navigation.View',
    requires: [
    	'Ext.navigation.View',
    	'rbdemo.view.ClientsInitialPanel',
    	'rbdemo.view.ClientsGroupListingViewPanel'
    ],
    
    xtype: 'xtypeClientsMainNavicationView',
    
    initialize: function () {
    	this.callParent(arguments);
        
        this.add([
        	//titleBar,
        	//twitterFeed
        ]);
	},
    
    config: {
    	iconCls: 'team02',
    	navigationBar: {
			cls: 'titlebar-black',
			id: 'idEventsNavigationBar',
			height:40,
			items : [
				{
					xtype:'button',
					text: '+',
					align: 'right',
					cls: 'titlebarbutton',
					id: 'idNavigationCreateClient',
					listeners: {
					   tap: {
							fn: function() {
								//console.log('Add Client: ' + this.parentView);
								this.up('xtypeClientsMainNavicationView').push({xtype:'xtypeClientsCreateClientFormPanel'});
								//this.setHidden(true);
						   }
					   }
				   }
				},
				{
					xtype:'button',
					id: 'idNavigationEditClient',
					text: 'Edit',
					align: 'right',
					hidden: true,
				},
			]
		},
        
    	items: [
        	{ xtype: 'xtypeClientsGroupListingViewPanel' }
        ]
    }
});