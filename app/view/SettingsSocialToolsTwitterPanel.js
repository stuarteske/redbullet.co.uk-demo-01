Ext.define("rbdemo.view.SettingsSocialToolsTwitterPanel", {
    extend: 'Ext.form.Panel',
    requires: [
    	'Ext.form.Panel',
    	'Ext.field.Select',
    	'Ext.field.DatePicker',
    	'Ext.field.Toggle',
    	'Ext.field.Email',
    	'Ext.field.Number',
    	'Ext.field.Password'
    ],
    
    xtype: 'xtypeSettingsSocialToolsTwitterPanel',
    
    config: {
    	title: 'Twitter Settings',
    	items: [
    		{
    			xtype:'fieldset',
    			title: 'User',
    			items: [
    				{
    					xtype: 'textfield',
						label: 'Username',
						name: 'username',
						placeHolder: 'Username',
					},
					{
    					xtype: 'passwordfield',
						label: 'Password',
						name: 'password',
						placeHolder: 'Password',
					},
    			],
    		},
    		{
    			xtype:'fieldset',
    			title: 'Settings',
    			items: [
    				{
						xtype: 'checkboxfield',
						name : 'ncc',
						label: 'Tweet Achievements',
						value: 'ncc',
						checked: false,
						labelWidth:'70%',
					},
					{
						xtype: 'checkboxfield',
						name : 'nuc',
						label: 'Tweet New Services',
						value: 'nuc',
						checked: false,
						labelWidth:'70%',
					},
    			],
    		},
    		{
    			xtype:	'fieldset',
    			items: [
    				{
						xtype:	'button',
						text:	'Save and Verify',
						ui:		'confirm',
					}
    			],
    		},
    	]
    }
});