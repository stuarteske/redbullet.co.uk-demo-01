Ext.define("rbdemo.view.SettingsViewServiceSprayTan", {
    extend: 'Ext.Panel',
    requires: [
    	'Ext.Panel',
    ],
    
    xtype: 'xtypeSettingsViewServiceSprayTan',
    
    initialize: function () {
    	this.callParent(arguments);
        
        this.add([]);
	},
    
    config: {
    	title: 'View Service',
    	layout:'vbox',
		scrollable:true,
		items: [
			{
				xtype:'panel',
				height:'10px',
			},
			// Photo
			{
				xtype: 'panel',
				html:'<div><div style="margin:0px 10px;border:solid 1px #ccc;padding:10px;"><div style="float:left;margin-right:10px;margin-top:-0.1em;"><img style="opacity:0.75;border:solid #ccc 1px;" src="resources/icons/photo_blank.png" width="48" height="48" /></div><span style="font-weight:bold;font-size:0.85em;">Spray Tan</span><br /><span style="font-weight:normal;font-size:0.7em;color:#666;">Category: Skin</span><div style="float:right;margin-top:-0.65em;"><img src="resources/images/dropout.png" /></div></div></div>',
			},
			// Description
			{
				xtype: 'panel',
				html:'<div><div style="margin:0px 10px;border:solid 1px #ccc;border-top:0px;padding:10px;"><div style="font-weight:normal;font-size:0.65em;color:#666;min-width:70px;float:left;">Description</div><div style="font-weight:normal;font-size:0.65em;color:#171717;">A great tanning product.</div></div></div>',
			},
			// Brands
			{
				xtype: 'panel',
				html:'<div><div style="margin:0px 10px;border:solid 1px #ccc;border-top:0px;padding:10px;"><div style="font-weight:normal;font-size:0.65em;color:#666;min-width:70px;float:left;">Brands</div><div style="font-weight:normal;font-size:0.65em;color:#171717;">L’Oreal, Wella, Minx<div style="float:right;margin-top:-0.45em;"><img src="resources/images/disclosure.png" /></div></div></div></div>',
			},
			// Price
			{
				xtype: 'panel',
				html:'<div><div style="margin:0px 10px;border:solid 1px #ccc;border-top:0px;padding:10px;"><div style="font-weight:normal;font-size:0.65em;color:#666;min-width:70px;float:left;">Price</div><div style="font-weight:normal;font-size:0.65em;color:#171717;">£15<div style="float:right;margin-top:-0.45em;"><img src="resources/images/disclosure.png" /></div></div></div></div>',
			},
			// Duration
			{
				xtype: 'panel',
				html:'<div><div style="margin:0px 10px;border:solid 1px #ccc;border-top:0px;padding:10px;"><div style="font-weight:normal;font-size:0.65em;color:#666;min-width:70px;float:left;">Duration</div><div style="font-weight:normal;font-size:0.65em;color:#171717;">35 Minutes<div style="float:right;margin-top:-0.45em;"><img src="resources/images/disclosure.png" /></div></div></div></div>',
			},
			// Availability
			{
				xtype: 'panel',
				html:'<div><div style="margin:0px 10px;border:solid 1px #ccc;border-top:0px;padding:10px;"><div style="font-weight:normal;font-size:0.65em;color:#666;min-width:70px;float:left;">Availability</div><div style="font-weight:normal;font-size:0.65em;color:#171717;">72 Hours<div style="float:right;margin-top:-0.45em;"><img src="resources/images/disclosure.png" /></div></div></div></div>',
			},
			// Prerequisites
			{
				xtype: 'panel',
				html:'<div><div style="margin:0px 10px;border:solid 1px #ccc;border-top:0px;padding:10px;"><div style="font-weight:normal;font-size:0.65em;color:#666;min-width:70px;float:left;">Required</div><div style="font-weight:normal;font-size:0.65em;color:#171717;">Consultation<div style="float:right;margin-top:-0.45em;"><img src="resources/images/disclosure.png" /></div></div></div></div>',
			},
			// Booking Conditions
			{
				xtype: 'panel',
				html:'<div><div style="margin:0px 10px;border:solid 1px #ccc;border-top:0px;padding:10px;"><div style="font-weight:normal;font-size:0.65em;color:#666;min-width:70px;float:left;">Conditions<br />&nbsp<br />&nbsp;</div><div style="font-weight:normal;font-size:0.65em;color:#171717;">New (confirmed) customer<br />Returning (confirmed) customer</div></div></div>',
			},
			{
				xtype: 'panel',
				height:10,
			},
		]
    }
});