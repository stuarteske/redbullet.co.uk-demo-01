Ext.define("rbdemo.view.EventsMainNavigationView", {
    extend: 'Ext.navigation.View',
    requires: [
    	'Ext.navigation.View',
    	'rbdemo.view.EventsInitialPanel',
    	'rbdemo.view.EventsCreateEventFormPanel',
    	'rbdemo.view.EventsPerDayListPanel',
    ],
    
    xtype: 'xtypeEventsMainNavicationView',
    
    initialize: function () {
    	this.callParent(arguments);
        
        this.add([
        ]);
	},
    
    config: {
    	iconCls: 'bookings02',
		navigationBar: {
			cls: 'titlebar-black',
			height:40,
			items : [
				{
					xtype:'button',
					text: '+',
					align: 'right',
					cls: 'titlebarbutton',
					id: 'idNavigationAddEvent',
					//listeners: {
					//   tap: {
					//		fn: function() {
								//console.log('Add Client: ' + this.parentView);
					//			this.up('xtypeEventsMainNavicationView').push({xtype:'xtypeEventsCreateEventFormPanel'});
								//this.setHidden(true);
					//	   }
					//   }
					//}
				},
				{
					xtype:'button',
					id: 'idNavigationEditEvent',
					text: 'Edit',
					align: 'right',
					hidden: true,
				},
			]
		},
		
    	items: [
        	{ xtype: 'xtypeEventsInitialPanel' }
        ]
    }
});