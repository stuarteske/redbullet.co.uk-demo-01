Ext.define("rbdemo.view.SettingsAddService", {
    extend: 'Ext.form.Panel',
    requires: [
    	'Ext.form.Panel',
    	'Ext.field.Select',
    	'Ext.field.DatePicker',
    	'Ext.field.Toggle',
    	'Ext.field.Email',
    	'Ext.field.Number'
    ],
    
    xtype: 'xtypeSettingsAddService',
    
    config: {
    	title: 'Add Service',
    	items: [
    		{
    			xtype:'fieldset',
    			title: 'Details',
    			items: [
    				{
    					xtype: 'selectfield',
						label: 'Category',
						name: 'category',
						options: [
							{text: 'Hair',	value: '1'},
							{text: 'Nails',	value: '2'},
							{text: 'Skin',	value: '3'},
						]
    				},
    				{
    					xtype: 'textfield',
						label: 'Title',
						name: 'title',
					},
					{
						xtype: 'textareafield',
						label: 'Description',
						maxRows: 2,
						name: 'description'
					},
					{
    					xtype: 'textfield',
						label: 'Brands',
						name: 'brands',
						placeHolder: 'e.g. Shellac, L’Oreal, Wella, Minx',
					},
					{
    					xtype: 'numberfield',
                    	label: 'Price (£)',
                    	minValue: 0,
                    	maxValue: 1000,
                    	name: 'price',
						placeHolder: '15',
					},
					{
    					xtype: 'numberfield',
                    	label: 'Duration (Minutes)',
                    	minValue: 0,
                    	maxValue: 1000,
                    	name: 'duration',
						placeHolder: '60',
					},
					{
    					xtype: 'numberfield',
                    	label: 'Notice (Hours)',
                    	minValue: 0,
                    	maxValue: 1000,
                    	name: 'notice',
						placeHolder: '72',
					},
					{
						xtype: 'textfield',
						label: 'Pre-requisites',
						name: 'prerequisites',
						placeHolder: 'e.g. a patch test or consultation.',
					},
    			],
    		},
    		{
    			xtype:'fieldset',
    			title: 'Booking Conditions',
    			items: [
    				{
						xtype: 'checkboxfield',
						name : 'ncc',
						label: 'New (confirmed) customers',
						value: 'ncc',
						checked: false,
						labelWidth:'70%',
					},
					{
						xtype: 'checkboxfield',
						name : 'rcc',
						label: 'Returning (confirmed) customers',
						value: 'rcc',
						checked: false,
						labelWidth:'70%',
					},
					{
						xtype: 'checkboxfield',
						name : 'nuc',
						label: 'New (unconfirmed) customers',
						value: 'nuc',
						checked: false,
						labelWidth:'70%',
					},
					{
						xtype: 'checkboxfield',
						name : 'ruc',
						label: 'Returning (unconfirmed) customers',
						value: 'ruc',
						checked: false,
						labelWidth:'70%',
					},
    			],
    		},
    		{
    			xtype:	'fieldset',
    			items: [
    				{
						xtype:	'button',
						text:	'Add Photo',
						ui:		'normal',
					}
    			],
    		},
    		{
    			xtype:	'fieldset',
    			items: [
    				{
						xtype:	'button',
						text:	'Create',
						ui:		'confirm',
					}
    			],
    		},
    	]
    }
});