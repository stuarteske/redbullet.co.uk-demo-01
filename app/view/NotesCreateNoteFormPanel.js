Ext.define("rbdemo.view.NotesCreateNoteFormPanel", {
    extend: 'Ext.form.Panel',
    requires: [
    	'Ext.form.Panel',
    	'Ext.field.Select',
    	'Ext.field.DatePicker',
    	'Ext.field.Toggle',
    	'Ext.field.Email'
    ],
    
    xtype: 'xtypeNotesCreateNoteFormPanel',
    
    config: {
    	title: 'Add Note',
    	items: [
    		{
    			xtype:'fieldset',
    			items: [
    				{
						xtype: 'textareafield',
						label: 'Note',
						maxRows: 8,
						name: 'note'
					}
    			],
    		},
    		{
    			xtype:	'fieldset',
    			items: [
    				{
						xtype: 	'button',
						itemId:	'itemIdPhotoPickerBtn',
						text: 	'Photo Picker',
					},
    			],
    		},
    		{
    			xtype:	'fieldset',
    			items: [
    				{
						xtype:	'button',
						text:	'Create',
						ui:		'confirm',
					}
    			],
    		},
    	]
    }
});