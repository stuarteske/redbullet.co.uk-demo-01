Ext.define("rbdemo.view.MainTabPanel", {
    extend: 'Ext.TabPanel',
    requires: [
    	'Ext.TabPanel',
    	'rbdemo.view.ActivityNavigationView',
    ],
    
    xtype: 'xtypeMainTabPanel',
    
    initialize: function () {
    	this.callParent(arguments);
	},
    
    config: {
        tabBarPosition: 'bottom',
        cls:'idAppTabbar',
        tabBar: {
        	id:'idAppTabBar',
        },
        
        items: [
        	{ xtype: 'xtypeActivityNavicationView' },
        	{ xtype: 'xtypeClientsMainNavicationView' },
        	{ xtype: 'xtypeEventsMainNavicationView' },
        	{ xtype: 'xtypeSettingsMainNavicationView' }
        ]
    }
});