Ext.define("rbdemo.view.ActivityContainingPanel", {
    extend: 'Ext.Panel',
    requires: [
    	'Ext.Panel',
    	'Ext.TitleBar',
    	'Ext.Button',
    	'Ext.SegmentedButton',
    	'Ext.NavigationView',
    	'Ext.field.Radio',
    	'Ext.form.Panel',
    	'Ext.form.FieldSet',
    ],
    
    xtype: 'xtypeActivityContainingPanel',
    
    initialize: function () {
    	this.callParent(arguments);
    	
    	var activityTitlebar = { 
			xtype: 'titlebar',
			cls: 'titlebar-black',
			docked: 'top',
			title: 'Activity',
			height:40,
			items : [
				{
					xtype:'button',
					text: 'Filter',
					align: 'right',
					cls: 'titlebarbutton',
					listeners: {
					   tap: {
							fn: function() {
							   //console.log( rbdemo.globals['key'] );
							   rbdemo.globals['key'].show();
						   }
					   }
				   }
				}
			]
		}
    	
    	var activityToolbar = {
			xtype : 'toolbar',
			cls:'toolbar-grey',
			docked: 'top',
			height: 40,
			layout:{
				pack: 'center'
			},
			items: [
				{
					xtype: 'segmentedbutton',
					allowMultiple: false,
					height: 22,
					allowDepress:false,
					items: [
						{
							text: 'Notifications',
							id:'idNotification',
							pressed: true,
							cls: 'toolbarbutton',
						},
						{
							text: 'Timeline',
							id: 'idTimeline',
							cls: 'toolbarbutton',
						}
					],
					listeners: {
						toggle: function(container, button, pressed){
							//console.log("User toggled the '" + button.getText() + "' button: " + (pressed ? 'on' : 'off'));
							
							switch (button.getId()) {
								case 'idNotification':
									//console.log('Notification');
									navigationView.animateActiveItem(0, {type: 'slide', direction: 'right'});
									break;
								default:
									//console.log('tasks');
									navigationView.animateActiveItem(1, {type: 'slide', direction: 'left'});
									break;
							}
						}
					}
				},
			]
		}
		
		var navigationView = Ext.create('Ext.NavigationView', {
			//xtype: 'navigationview',
			id: 'idActivityListContainer',
			flex:1,
			//fullscreen:true,
			navigationBar: {
				hidden: true,
			},
		});	
		
		var notificationList = Ext.create('Ext.List', {
			//xtype: 'list',
			itemId:'itemIdNotificationList',
			styleHtmlContent:true,
			styleHtmlCls:'listclass',
			emptyText: "Check your internet connection and try again.",
			loadingText: 'Loading...',
			baseCls: 'activity-notifications-list',
			onItemDisclosure: false,
			//itemTpl: ['<div class="itemimagecontainer"><img src="{icon}" /></div><div style="margin-left:60px;"><h6>{type}</h6><p><strong>{emphasis}</strong> {description}</p></div>'],
			itemTpl: ['<div class="itemimagecontainer"><img src="{icon}" /></div><img style="float:right;margin-top:0.2em;" src="resources/images/disclosure.png" /><div style="margin-left:45px;width:75%;"><h6 class="item-header">{type}</h6><p class="item-body"><span class="item-emphasis">{emphasis}</span> {description}</p></div>'],
			data: [
				{ type: 'General Alert',  emphasis: 'Appointment', description: 'with Julie Adams today at 16:00.', icon: 'resources/icons/meeting.png' },
				{ type: 'Statistic', emphasis: '20% increase', description: 'in spray tans this month.', icon: 'resources/icons/graph.png' },
				//{ type: 'Studio Calendar Alert',  emphasis: 'Team meeting at 14:00 today.', description: '', icon: 'resources/icons/meeting.png' },
				{ type: 'Achievement',  emphasis: '5 Bookings', description: 'made this week.', icon: 'resources/icons/star.png' },
				{ type: 'Twitter',  emphasis: '3 New Follower', description: 'this week.', icon: 'resources/icons/star.png' },
				{ type: 'Facebook',  emphasis: '5 New Friends', description: 'this week.', icon: 'resources/icons/star.png' },
			],
		});
		
		var timelineList = Ext.create('Ext.dataview.List', {
			//xtype: 'list',
			fullscreen: true,
			itemId:'itemIdTimelineList',
			styleHtmlContent:true,
			styleHtmlCls:'timeline-list',
			onItemDisclosure: true,
			itemTpl: '<div><p style="font-size:0.6em;color:#666;padding-bottom:2px;"><span style="display:block;float:left;margin-right:4px;width:34px;text-align:center;">{notification_time}</span> <span>{notification_type}</span></p><p><div style="float:left;padding:4px 4px 0px 0px;" ><img src="{notification_icon}" /></div> <span style="font-size:0.8em;">{notification_description}</span></p><p style="font-size:0.6em;padding-top:3px;">{notification_spec}</p></div>',
			store: 'ActivityTimelineStore',
			grouped: false,
		});
		
		navigationView.push(notificationList);
		navigationView.push(timelineList);
		navigationView.setActiveItem(0);
		
		this.add([
        	//activityTitlebar,
        	activityToolbar,
        	navigationView,
        ]);
    },
    
    config: {
    	title:'Activity',
    	layout: 'vbox',
        items: [],
    }
});