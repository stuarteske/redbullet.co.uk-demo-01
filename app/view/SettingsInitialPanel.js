Ext.define("rbdemo.view.SettingsInitialPanel", {
    extend: 'Ext.Panel',
    requires: [
    	'Ext.Panel',
    	'Ext.dataview.List',
    	'Ext.data.Store'
    ],
    
    xtype: 'xtypeSettingsInitialPanel',
    
    config: {
    	title: 'Settings',
    	fullscreen: true,
    	layout:'card',
    	items: [
    		{
				xtype: 'list',
				onItemDisclosure: true,
				itemTpl: '{title}',
				grouped:true,
				store: 'SettingsDataStore',
    		}
    	]
    }
});