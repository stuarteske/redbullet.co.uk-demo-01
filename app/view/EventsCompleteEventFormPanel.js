Ext.define("rbdemo.view.EventsCompleteEventFormPanel", {
    extend: 'Ext.form.Panel',
    requires: [
    	'Ext.form.Panel',
    	'Ext.field.Select',
    	'Ext.field.DatePicker',
    	'Ext.field.Toggle'
    ],
    
    xtype: 'xtypeEventsCompleteEventFormPanel',
    
    config: {
    	title: 'Event Completed',
    	items: [
    		{
    			xtype:'fieldset',
    			items: [
					{
    					xtype:	'textfield',
						label:	'Name',
						name:	'name',
						value:	'Julie Adams',
						disabled:true,
					},
					{
    					xtype:	'textfield',
						label:	'Time',
						name:	'time',
						value:	'53 Mintues',
						disabled:true,
					},
					{
    					xtype: 'selectfield',
						label: 'Rebook',
						name: 'rebook',
						options: [
							{text: '',  value: '1'},
							{text: '1 Day', value: '2'},
							{text: '2 Days', value: '3'},
							{text: '3 Days', value: '4'},
							{text: '4 Days', value: '4'},
							{text: '5 Days', value: '4'},
							{text: '6 Days', value: '4'},
							{text: '7 Days', value: '4'},
							{text: '2 Weeks', value: '4'},
							{text: '3 Weeks', value: '4'},
							{text: '4 Weeks', value: '4'},
						]
					},
					{
						xtype: 'textfield',
						label: 'Availability',
						name: 'avalibility',
						disabled:true,
						value:'Available (Indicator)',
					},
					{
						xtype: 'button',
						text: 'View Calendar',
						ui:'normal',
					},
    			],
    		},
    		{
    			xtype: 'fieldset',
    			title: 'Finance',
    			items: [
    				{
    					xtype:	'textfield',
						label:	'Price',
						name:	'price',
						value:	'£35',
					},
					{
    					xtype:	'textfield',
						label:	'Tip',
						name:	'tip',
						value:	'£5',
					},
					{
    					xtype:	'textfield',
						label:	'Total',
						name:	'total',
						value:	'£40',
					},
    			],
    		},
    		{
    			xtype:'fieldset',
    			items: [
    				{
						xtype: 'button',
						text: 'Completed',
						ui:'confirm',
					}
    			],
    		},
    	]
    }
});