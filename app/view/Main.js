Ext.define("rbdemo.view.Main", {
    extend: 'Ext.Container',
    requires: [
    	'Ext.Container',
    	'rbdemo.view.MainTabPanel',
    	'rbdemo.view.SettingsMainNavigationView',
    	'rbdemo.view.EventsMainNavigationView',
    	'rbdemo.view.ClientsMainNavigationView'
    	
    ],
    
    initialize: function () {
    	this.callParent(arguments);
	},
    
    config: {
        xtype: 'xtypeMainContainer',
        fullscreen: true,
        id: 'idMainContainer',
        
        layout: {
        	type: 'card'
        },
        
        items: [
        	{ xtype: 'xtypeMainTabPanel' }
        ]
    }
});