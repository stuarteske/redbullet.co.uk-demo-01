Ext.define("rbdemo.view.SettingsMainNavigationView", {
    extend: 'Ext.navigation.View',
    requires: [
    	'Ext.navigation.View',
    	'rbdemo.view.SettingsInitialPanel'
    ],
    
    xtype: 'xtypeSettingsMainNavicationView',
    
    initialize: function () {
    	this.callParent(arguments);
        
        this.add([
        	//titleBar,
        	//twitterFeed
        ]);
	},
    
    config: {
    	iconCls: 'settings02',
    	navigationBar: {
			cls: 'titlebar-black',
			height:40,
			items : [
				{
					xtype:'button',
					itemId:'logoutBtn',
					text: 'Logout',
					align: 'right',
					cls: 'titlebarbutton',
				}
			]
		},
        
    	items: [
        	{ xtype: 'xtypeSettingsInitialPanel' }
        ]
    }
});