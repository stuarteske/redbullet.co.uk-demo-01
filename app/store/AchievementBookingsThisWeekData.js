Ext.define('rbdemo.store.AchievementBookingsThisWeekData', {
    extend: 'Ext.data.Store',
    
    requires: ['Ext.data.proxy.LocalStorage'],
    
    config: {
    	storeId: 'AchievementBookingThisWeekStore',
        model: 'rbdemo.model.AchievementBookingsThisWeek',
        autoLoad: true,
        sorters: 'weight',
        
        grouper: {
		   	groupFn: function(record) {
			   	return record.get('group');
		   	}
	   	},
	   	groupDir: 'DESC',
        
        data: [
			
			{ 
				service: 'Spray Tan',
				group: 'This Week',
				icon: 'resources/icons/bookings.png',
				appointment:'Appointment 25/07/2012',
				weight: 5
			},
			{
				service: 'Manicure',
				group: 'This Week',
				icon: 'resources/icons/bookings.png',
				appointment:'Appointment 24/07/2012',
				weight: 4 
			},
			{
				service: 'Spray Tan',
				group: 'This Week',
				icon: 'resources/icons/bookings.png',
				appointment:'Appointment 23/07/2012',
				weight: 3
			},
			{ 
				service: 'Manicure',
				group: 'This Week',
				icon: 'resources/icons/bookings.png',
				appointment:'Appointment 22/07/2012',
				weight: 2
			},
			{
				service: 'Spray Tan',
				group: 'This Week',
				icon: 'resources/icons/bookings.png',
				appointment:'Appointment 21/07/2012',
				weight: 1 
			},
			{ service: 'Manicure',	group: 'Last Week', icon: 'resources/icons/bookings.png', appointment:'Appointment 19/07/2012', weight: 4  },
			{ service: 'Spray Tan',	group: 'Last Week', icon: 'resources/icons/bookings.png', appointment:'Appointment 18/07/2012', weight: 3  },
			{ service: 'Manicure',	group: 'Last Week', icon: 'resources/icons/bookings.png', appointment:'Appointment 15/07/2012', weight: 2  },
			{ service: 'Spray Tan',	group: 'Last Week', icon: 'resources/icons/bookings.png', appointment:'Appointment 14/07/2012', weight: 1  },
	   ]
    }
});