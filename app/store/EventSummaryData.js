Ext.define('rbdemo.store.EventSummaryData', {
    extend: 'Ext.data.Store',
    
    requires: [
    	'Ext.data.proxy.LocalStorage',
    	'rbdemo.model.EventDate'	
    ],
    
    config: {
    	storeId: 'EventSummaryDataStore',
        model: 'rbdemo.model.EventDate',
        autoLoad: true,
        grouper: {
		   	groupFn: function(record) {
			   	return record.get('month');
		   	}
	   	},
        groupDir: 'ASC',
        sorter: 'nday',
        data: [
			{	day: '23 MON',	nday: '23/07/12',	month: '07 July', 		bookings: '3',	image: 'resources/icons/slice2.png'	},
			{	day: '24 TUE',	nday: '24/07/12',	month: '07 July', 		bookings: '1',	image: 'resources/icons/slice1.png'	},
			{	day: '25 WED',	nday: '25/07/12',	month: '07 July', 		bookings: '7',	image: 'resources/icons/slice3.png'	},
			{	day: '26 THU',	nday: '26/07/12',	month: '07 July', 		bookings: '2',	image: 'resources/icons/slice1.png'	},
			{	day: '27 FRI',	nday: '27/07/12',	month: '07 July', 		bookings: '6',	image: 'resources/icons/slice5.png'	},
			{	day: '28 SAT',	nday: '28/07/12',	month: '07 July', 		bookings: '1',	image: 'resources/icons/slice1.png'	},
			{	day: '29 SUN',	nday: '29/07/12',	month: '07 July', 		bookings: '9',	image: 'resources/icons/slice6.png'	},
			{	day: '30 MON',	nday: '30/07/12',	month: '07 July', 		bookings: '12',	image: 'resources/icons/slice8.png'	},
			{	day: '31 TUE',	nday: '31/07/12',	month: '07 July',		bookings: '4',	image: 'resources/icons/slice2.png'	},
			{	day: '1 WED',	nday: '01/08/12',	month: '08 August', 	bookings: '8',	image: 'resources/icons/slice5.png'	},
			{	day: '2 THU',	nday: '02/08/12',	month: '08 August', 	bookings: '2',	image: 'resources/icons/slice1.png'	},
			{	day: '3 FRI',	nday: '03/08/12',	month: '08 August', 	bookings: '9',	image: 'resources/icons/slice4.png'	},
			{	day: '4 SAT',	nday: '04/08/12',	month: '08 August', 	bookings: '2',	image: 'resources/icons/slice2.png'	},
			{	day: '5 SUN - Today',	nday: '05/08/12',	month: '08 August', 	bookings: '8',	image: 'resources/icons/slice4.png'	},
			{	day: '6 MON - Blocked Out',	nday: '06/08/12',	month: '08 August', 	bookings: '0',	image: 'resources/icons/slice8.png'	},
			{	day: '7 TUE',	nday: '07/08/12',	month: '08 August', 	bookings: '2',	image: 'resources/icons/slice1.png'	},
			{	day: '8 WED',	nday: '08/08/12',	month: '08 August', 	bookings: '9',	image: 'resources/icons/slice4.png'	},
			{	day: '9 THU',	nday: '09/08/12',	month: '08 August', 	bookings: '2',	image: 'resources/icons/slice2.png'	},
			{	day: '10 FRI',	nday: '10/08/12',	month: '08 August', 	bookings: '8',	image: 'resources/icons/slice4.png'	},
			{	day: '11 SAT',	nday: '11/08/12',	month: '08 August', 	bookings: '8',	image: 'resources/icons/slice5.png'	},
			{	day: '12 SUN',	nday: '12/08/12',	month: '08 August', 	bookings: '2',	image: 'resources/icons/slice1.png'	},
			{	day: '13 MON',	nday: '13/08/12',	month: '08 August', 	bookings: '9',	image: 'resources/icons/slice4.png'	},
			{	day: '14 TUE',	nday: '14/08/12',	month: '08 August', 	bookings: '2',	image: 'resources/icons/slice2.png'	},
			{	day: '15 WED',	nday: '15/08/12',	month: '08 August', 	bookings: '8',	image: 'resources/icons/slice4.png'	},
			{	day: '16 THU',	nday: '16/08/12',	month: '08 August', 	bookings: '8',	image: 'resources/icons/slice5.png'	},
			{	day: '17 FRI',	nday: '17/08/12',	month: '08 August', 	bookings: '2',	image: 'resources/icons/slice1.png'	},
			{	day: '18 SAT',	nday: '18/08/12',	month: '08 August', 	bookings: '9',	image: 'resources/icons/slice4.png'	},
			{	day: '19 SUN',	nday: '19/08/12',	month: '08 August', 	bookings: '2',	image: 'resources/icons/slice2.png'	},
			{	day: '20 MON',	nday: '20/08/12',	month: '08 August', 	bookings: '8',	image: 'resources/icons/slice4.png'	},
			{	day: '21 TUE',	nday: '21/08/12',	month: '08 August', 	bookings: '8',	image: 'resources/icons/slice5.png'	},
			{	day: '22 WED',	nday: '22/08/12',	month: '08 August', 	bookings: '2',	image: 'resources/icons/slice1.png'	},
			{	day: '23 THU',	nday: '23/08/12',	month: '08 August', 	bookings: '9',	image: 'resources/icons/slice4.png'	},
			{	day: '24 FRI',	nday: '24/08/12',	month: '08 August', 	bookings: '2',	image: 'resources/icons/slice2.png'	},
			{	day: '25 SAT',	nday: '25/08/12',	month: '08 August', 	bookings: '8',	image: 'resources/icons/slice4.png'	},
			{	day: '26 SUN',	nday: '26/08/12',	month: '08 August', 	bookings: '8',	image: 'resources/icons/slice5.png'	},
			{	day: '27 MON',	nday: '27/08/12',	month: '08 August', 	bookings: '2',	image: 'resources/icons/slice1.png'	},
			{	day: '28 TUE',	nday: '28/08/12',	month: '08 August', 	bookings: '9',	image: 'resources/icons/slice4.png'	},
			{	day: '29 WED',	nday: '29/08/12',	month: '08 August', 	bookings: '2',	image: 'resources/icons/slice2.png'	},
			{	day: '30 THU',	nday: '30/08/12',	month: '08 August', 	bookings: '8',	image: 'resources/icons/slice4.png'	},
			{	day: '31 FRI',	nday: '31/08/12',	month: '08 August', 	bookings: '8',	image: 'resources/icons/slice4.png'	},
			{	day: '01 SAT',	nday: '01/09/12',	month: '09 September', 	bookings: '8',	image: 'resources/icons/slice5.png'	},
			{	day: '02 SUN',	nday: '02/09/12',	month: '09 September', 	bookings: '2',	image: 'resources/icons/slice1.png'	},
			{	day: '03 MON',	nday: '03/09/12',	month: '09 September', 	bookings: '9',	image: 'resources/icons/slice4.png'	},
			{	day: '04 TUE',	nday: '04/09/12',	month: '09 September', 	bookings: '2',	image: 'resources/icons/slice2.png'	},
			{	day: '05 WED',	nday: '05/09/12',	month: '09 September', 	bookings: '8',	image: 'resources/icons/slice4.png'	},
	   ]
    }
});