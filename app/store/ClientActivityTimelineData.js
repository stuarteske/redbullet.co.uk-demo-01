Ext.define('rbdemo.store.ClientActivityTimelineData', {
    extend: 'Ext.data.Store',
    
    requires: ['Ext.data.proxy.LocalStorage'],
    
    config: {
    	storeId: 'ClientActivityTimelineStore',
        model: 'rbdemo.model.ActivityTimelineEntity',
        autoLoad: true,
        sorters: 'group',
        
        grouper: {
		   	groupFn: function(record) {
			   	return record.get('group');
		   	}
	   	},
        
        data: [
			{
				notification_time:'12:56',
				notification_icon:'resources/icons/confirmed.png',
				notification_type:'Confirmation sent by you',
				notification_description:'Appointment Confirmed',
				notification_spec:'24/06/12 @ 10:00'
			},
			{
				notification_time:'12:36',
				notification_icon:'resources/icons/bookings.png',
				notification_type:'Appointment booked by you',
				notification_description:'02/11 Carr Aldred',
				notification_spec:'Spray Tan with You'
			},
			{
				notification_time:'11:12',
				notification_icon:'resources/icons/bookings.png',
				notification_type:'Appointment booked by Anna',
				notification_description:'23/10 Carr Aldred',
				notification_spec:'Spray Tan with Anna'
			},
			{
				notification_time:'09:50',
				notification_icon:'resources/icons/note.png',
				notification_type:'Customer Records updated',
				notification_description:'Carr Aldred',
				notification_spec:'Hair Colour Notes'
			},
	   ]
    }
});