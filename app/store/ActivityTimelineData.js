Ext.define('rbdemo.store.ActivityTimelineData', {
    extend: 'Ext.data.Store',
    
    requires: ['Ext.data.proxy.LocalStorage'],
    
    config: {
    	storeId: 'ActivityTimelineStore',
        model: 'rbdemo.model.ActivityTimelineEntity',
        autoLoad: true,
        sorters: 'group',
        
        grouper: {
		   	groupFn: function(record) {
			   	return record.get('group');
		   	}
	   	},
        
        data: [
			{
				notification_time:'12:56',
				notification_icon:'resources/icons/phone.png',
				notification_type:'Notification sent by you',
				notification_description:'SMS sent to Barry Jones',
				notification_spec:'24/06/12 @ 10:00'
			},
			{
				notification_time:'12:36',
				notification_icon:'resources/icons/bookings.png',
				notification_type:'Appointment booked by you',
				notification_description:'28/09 Julie Adams',
				notification_spec:'Spray Tan with You'
			},
			{
				notification_time:'11:12',
				notification_icon:'resources/icons/note.png',
				notification_type:'Appointment in progress',
				notification_description:'Barry Jones',
				notification_spec:'Spray Tan with You'
			},
			{
				notification_time:'09:50',
				notification_icon:'resources/icons/personplus.png',
				notification_type:'Customer added by you',
				notification_description:'Julie Adams',
				notification_spec:'Studio Customer'
			},
	   ]
    }
});