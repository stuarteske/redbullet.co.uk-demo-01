Ext.define('rbdemo.store.Settings', {
    extend: 'Ext.data.Store',
    
    requires: ['Ext.data.proxy.LocalStorage'],
    
    config: {
    	storeId: 'SettingsDataStore',
        model: 'rbdemo.model.Settings',
        
        grouper: {
		   	groupFn: function(record) {
			   	return record.get('group');
		   	}
	   	},
	   	groupDir: 'DESC',
        
        data: [
        	{ 
				title: 'Manicure',
				group: 'Services',
			},
        	{ 
				title: 'Spray Tan',
				group: 'Services',
			},
			{ 
				title: 'Facebook',
				group: 'Social Tools',
			},
			{ 
				title: 'Twitter',
				group: 'Social Tools',
			},
			{ 
				title: 'Your Profile',
				group: 'Your Settings',
			},
			{ 
				title: 'Add Service',
				group: 'Your Settings',
			},
			/*{ 
				title: 'Add Orgnisation',
				group: 'Your Settings',
			},
			{ 
				title: 'Advanced Settings',
				group: 'Your Settings',
			},*/
	   ]
    }
});