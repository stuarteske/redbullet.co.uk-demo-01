Ext.define('rbdemo.store.ClientData', {
    extend: 'Ext.data.Store',
    
    requires: ['Ext.data.proxy.LocalStorage'],
    
    config: {
    	storeId: 'ClientDataStore',
        model: 'rbdemo.model.ClientEntity',
        autoLoad: true,
        sorters: 'surname',
        
        grouper: {
		   	groupFn: function(record) {
			   	return record.get('surname')[0];
		   	}
	   	},
        
        data: [
			{ 
				firstname: 'George',
				surname: 'Ali',
				icon: 'resources/icons/guest.png',
				appointment:'Last appointment 05/07/2012'
			},
			{
				firstname: 'Carr',
				surname: 'Aldred',
				icon: 'resources/icons/guest.png',
				appointment:'Last appointment 05/07/2012'
			},
			{
				firstname: 'Ryan',
				surname: 'Ashley',
				icon: 'resources/icons/guest.png',
				appointment:'Last appointment 05/07/2012'
			},
			{ 
				firstname: 'Sarah',
				surname: 'Amos',
				icon: 'resources/icons/guest.png',
				appointment:'Last appointment 05/07/2012'
			},
			{
				firstname: 'Fatima',
				surname: 'Allison',
				icon: 'resources/icons/guest.png',
				appointment:'Last appointment 05/07/2012'
			},
			{ firstname: 'Yasemin',	surname: 'Balshaw', icon: 'resources/icons/guest.png', appointment:'Last appointment 05/07/2012' },
			{ firstname: 'Nathan',	surname: 'Bamber', icon: 'resources/icons/guest.png', appointment:'Last appointment 05/07/2012' },
			{ firstname: 'Louis',	surname: 'Barrow', icon: 'resources/icons/guest.png', appointment:'Last appointment 05/07/2012' },
			{ firstname: 'Hugo',	surname: 'Baskin', icon: 'resources/icons/guest.png', appointment:'Last appointment 05/07/2012' },
			{ firstname: 'Tom',	surname: 'Bayton', icon: 'resources/icons/guest.png', appointment:'Last appointment 05/07/2012' }
	   ]
    }
});