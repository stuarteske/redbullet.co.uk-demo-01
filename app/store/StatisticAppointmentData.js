Ext.define('rbdemo.store.StatisticAppointmentData', {
    extend: 'Ext.data.Store',
    
    requires: ['Ext.data.proxy.LocalStorage'],
    
    config: {
    	storeId: 'StatisticAppointmentStore',
        model: 'rbdemo.model.StatisticAppointmentEntity',
        autoLoad: true,
        sorters: 'weight',
        
        grouper: {
		   	groupFn: function(record) {
			   	return record.get('group');
		   	}
	   	},
	   	groupDir: 'DESC',
        
        data: [
			
			{ 
				service: 'Spray Tan',
				group: 'This Month',
				icon: 'resources/icons/bookings.png',
				appointment:'Appointment 26/07/2012',
				weight: 5
			},
			{
				service: 'Spray Tan',
				group: 'This Month',
				icon: 'resources/icons/bookings.png',
				appointment:'Appointment 24/07/2012',
				weight: 4 
			},
			{
				service: 'Spray Tan',
				group: 'This Month',
				icon: 'resources/icons/bookings.png',
				appointment:'Appointment 20/07/2012',
				weight: 3
			},
			{ 
				service: 'Spray Tan',
				group: 'This Month',
				icon: 'resources/icons/bookings.png',
				appointment:'Appointment 16/07/2012',
				weight: 2
			},
			{
				service: 'Spray Tan',
				group: 'This Month',
				icon: 'resources/icons/bookings.png',
				appointment:'Appointment 13/07/2012',
				weight: 1 
			},
			{ service: 'Spray Tan',	group: 'This Month', icon: 'resources/icons/bookings.png', appointment:'Appointment 03/07/2012', weight: 0 },
			{ service: 'Spray Tan',	group: 'Last Month', icon: 'resources/icons/bookings.png', appointment:'Appointment 30/06/2012', weight: 5 },
			{ service: 'Spray Tan',	group: 'Last Month', icon: 'resources/icons/bookings.png', appointment:'Appointment 22/06/2012', weight: 4  },
			{ service: 'Spray Tan',	group: 'Last Month', icon: 'resources/icons/bookings.png', appointment:'Appointment 18/06/2012', weight: 3  },
			{ service: 'Spray Tan',	group: 'Last Month', icon: 'resources/icons/bookings.png', appointment:'Appointment 17/06/2012', weight: 2  },
			{ service: 'Spray Tan',	group: 'Last Month', icon: 'resources/icons/bookings.png', appointment:'Appointment 09/06/2012', weight: 1  },
	   ]
    }
});